/*
Copyright 2018 Nicolas Jouanin

This file is part of Plasma.

Plasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License 3.

Plasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>
 */
package org.mascarene.matrix.client.r0.model

package object filter {
  case class PostFilterResponse(filter_id: String)
  case class FilterDefinition(
      event_fields: Option[List[String]],
      event_format: Option[String],
      presence: Option[EventFilter],
      account_data: Option[EventFilter],
      room: Option[RoomFilter]
  )
  case class EventFilter(
      limit: Int,
      not_senders: Option[List[String]],
      not_types: Option[List[String]],
      senders: Option[List[String]],
      types: Option[List[String]]
  )
  case class RoomFilter(
      not_rooms: Option[List[String]],
      rooms: Option[List[String]],
      ephemeral: Option[RoomEventFilter],
      include_leave: Option[Boolean],
      state: Option[RoomEventFilter],
      timeline: Option[RoomEventFilter],
      account_data: Option[RoomEventFilter]
  )
  case class RoomEventFilter(
      limit: Int,
      not_senders: Option[List[String]],
      not_types: Option[List[String]],
      senders: Option[List[String]],
      types: Option[List[String]],
      lazy_load_members: Option[Boolean],
      include_redundant_members: Option[Boolean],
      not_rooms: Option[List[String]],
      rooms: Option[List[String]],
      contains_url: Option[Boolean]
  )

  case class GetFilterResponse(
      event_fields: Option[List[String]],
      event_format: String,
      presence: FilterDefinition,
      account_data: FilterDefinition,
      room: RoomFilter
  )
}
