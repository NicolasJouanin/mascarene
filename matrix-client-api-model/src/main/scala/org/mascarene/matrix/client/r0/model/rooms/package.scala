/*
Copyright 2018 Nicolas Jouanin

This file is part of Plasma.

Plasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License 3.

Plasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>
 */
package org.mascarene.matrix.client.r0.model

import io.circe.Json

package object rooms {
  case class CreateRoomRequest(
      preset: Option[String] = None,
      visibility: Option[String] = None,
      room_alias_name: Option[String] = None,
      name: Option[String] = None,
      topic: Option[String] = None,
      invite: Option[List[String]] = None,
      invite_3pid: Option[List[Invite3Pid]] = None,
      room_version: Option[String] = None,
      creation_content: Option[CreationContent] = None,
      initial_state: Option[List[StateEvent]] = None,
      is_direct: Boolean,
      power_level_content_override: Option[PowerLevelEventContent] = None
  )

  case class CreationContent(
      creator: String,
      `m.federate`: Option[Boolean] = Some(true),
      room_version: Option[String] = Some("1"),
      predecessor: Option[PreviousRoom] = None
  )

  case class PreviousRoom(room_id: String, event_id: String)

  case class Invite3Pid(id_server: String, medium: String, address: String)
  case class StateEvent(`type`: String, state_key: String, content: Option[Json] = None)
  case class PowerLevelEventContent(
      ban: Int,
      events: Map[String, Int] = Map.empty,
      events_default: Int,
      invite: Int,
      kick: Int,
      redact: Int,
      state_default: Int,
      users: Map[String, Int],
      users_default: Option[Int],
      notifications: Map[String, Int]
  )

  case class CreateRoomResponse(room_id: String)

  case class CreateRoomAliasRequest(room_id: String)

  case class ResolveRoomResponse(room_id: String, servers: List[String])

  case class JoinedRoomsResponse(joined_rooms: List[String])

  case class InviteRoomRequest(
      user_id: Option[String] = None,
      id_server: Option[String] = None,
      medium: Option[String] = None,
      address: Option[String] = None
  )

  case class JoinRoomRequest(third_party_signed: ThirdPartySigned)
  case class ThirdPartySigned(
      sender: String,
      mixid: String,
      token: String,
      signatures: Map[String, Map[String, String]]
  )
  case class JoinRoomResponse(room_id: String)

  case class KickUserRequest(user_id: String, reason: Option[String])
  case class BanUserRequest(user_id: String, reason: Option[String])
  case class UnBanUserRequest(user_id: String)

  case class ListPublicRoomsResponse(
      chunk: List[PublicRoomsChunk],
      next_batch: Option[String],
      prev_batch: Option[String],
      total_room_count_estimate: Option[Int]
  )

  case class PublicRoomsChunk(
      aliases: Option[List[String]],
      canonical_alias: Option[String],
      name: Option[String],
      num_joined_members: Int,
      room_id: String,
      topic: Option[String],
      world_readable: Boolean,
      guest_can_join: Boolean,
      avatar_url: Option[String]
  )

  case class PublicRoomsWithFilterRequest(limit: Option[Int], since: Option[String], filter: Option[Filter])
  case class Filter(generic_search_term: Option[String])

}
