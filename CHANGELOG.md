# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
#### client-server specification
Auth API endpoint implementation for:
- `GET /_matrix/client/versions` server endpoint.
- `GET /_matrix/client/r0/login` server endpoint.
- `POST /_matrix/client/r0/login` server endpoint.
- `POST /_matrix/client/r0/register` server endpoint.

