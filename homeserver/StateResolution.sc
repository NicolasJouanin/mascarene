sealed trait EventType

case object Create extends EventType

case object PowerLevels extends EventType

case object JoinRules extends EventType

case object Membership extends EventType

case object Topic extends EventType

case object Message extends EventType

case class Event(
    eventId: String,
    eventType: EventType,
    timeStamp: Int,
    stateKey: Option[String],
    sender: String,
    content: String,
    prevEvents: List[Event],
    authEvents: List[Event],
    powerLevels: Option[Map[String, Int]]
) {

  override def toString: String = eventId

}

def newStateEvent() =
  Event(
    eventId = "",
    eventType = Create,
    timeStamp = 0,
    stateKey = Some(""),
    sender = "",
    content = "",
    prevEvents = List.empty,
    authEvents = List.empty,
    powerLevels = None
  )

def newMessageEvent() =
  Event(
    eventId = "",
    eventType = Message,
    timeStamp = 0,
    stateKey = None,
    sender = "",
    content = "",
    prevEvents = List.empty,
    authEvents = List.empty,
    powerLevels = None
  )

def join(user: String) =
  newStateEvent().copy(eventType = Membership, sender = user, stateKey = Some(user), content = "join")
def leave(user: String) =
  newStateEvent().copy(eventType = Membership, sender = user, stateKey = Some(user), content = "leave")
def invites(u1: String, u2: String) =
  newStateEvent().copy(eventType = Membership, sender = u1, stateKey = Some(u2), content = "invite")
def kicks(u1: String, u2: String) =
  newStateEvent().copy(eventType = Membership, sender = u1, stateKey = Some(u2), content = "leave")
def invite(u1: String, u2: String) =
  newStateEvent().copy(eventType = Membership, sender = u1, stateKey = Some(u2), content = "bans")
def setsPowerLevels(user: String, pl: Map[String, Int]) =
  newStateEvent().copy(eventType = PowerLevels, sender = user, powerLevels = Some(pl))
def setsTopic(user: String, topic: String) = newStateEvent().copy(eventType = Topic, sender = user, content = topic)

type StateSet = Map[(EventType, String), Event]

def insertEvent(e: Event, stateSet: StateSet): StateSet = e match {
  case e @ Event(_, eventType, _, Some(stateKey), _, _, _, _, _) => stateSet + ((eventType, stateKey) -> e)
  case _                                                         => stateSet
}

def stateSetFromEventList(events: List[Event]) = {
  Map.from(events.map {
    case e @ Event(_, eventType, _, Some(stateKey), _, _, _, _, _) => ((eventType, stateKey) -> e)
  })
}

def authChain(e: Event): Set[Event] = e match {
  case Event(_, _, _, _, _, _, _, e, _) => Set.from(e).union(Set.from(e.flatMap(authChain)))
}

val create = newStateEvent().copy(eventId = "create", eventType = Create, sender = "@alice:example.com")
val alice_join =
  join("@alice:example.com").copy(eventId = "alice joins", prevEvents = List(create), authEvents = List(create))

val pl = setsPowerLevels("@alice:example.com", Map("@alice:example.com" -> 100))
  .copy(eventId = "power level", prevEvents = List(alice_join), authEvents = List(alice_join, create))

val join_rules = newStateEvent().copy(
  eventId = "join rules",
  eventType = JoinRules,
  sender = "@alice:example.com",
  content = "private",
  prevEvents = List(pl),
  authEvents = List(pl, alice_join, create)
)

val invite_bob = invites("@alice:example.com", "@bob:example.com").copy(
  eventId = "invite bob",
  prevEvents = List(join_rules),
  authEvents = List(pl, alice_join, create)
)

val invite_carol = invites("@alice:example.com", "@carol:example.com").copy(
  eventId = "invite carol",
  prevEvents = List(invite_bob),
  authEvents = List(pl, alice_join, create)
)

val bob_join = join("@bob:example.com").copy(
  eventId = "bob joins",
  prevEvents = List(invite_carol),
  authEvents = List(invite_bob, join_rules, create)
)

authChain(bob_join)

//is the sender in the room?
def isInRoom(sender: String, stateSet: StateSet): Boolean = {
  stateSet.get((Membership, sender)).fold(false)(event => event.content == "join")
}

//get the power levels (if any) from the state set
def stateSetPowerLevels(stateSet: StateSet): Option[Map[String, Int]] =
  stateSet.get((PowerLevels, "")).flatMap(_.powerLevels)

//determine if the user has sufficient power level
def hasPowerLevel(sender: String, powerLevels: Option[Map[String, Int]], pl: Int): Boolean = powerLevels match {
  case None              => true
  case Some(powerLevels) => powerLevels.getOrElse(sender, 0) >= pl
}

def insertMissingState(stateSet: StateSet, event: Event): StateSet = event match {
  case e @ Event(_, eventType, _, Some(stateKey), _, _, _, _, _) =>
    if (stateSet.contains((eventType, stateKey)))
      stateSet
    else
      stateSet + ((eventType, stateKey) -> e)
}

def augmentedStateSet(stateSet: StateSet, event: Event): StateSet = {
  event.authEvents.foldLeft(stateSet)((acc, e) => insertMissingState(acc, e))
}

//create events are only allowed if they are the first event
def isAuthorized(event: Event, stateSet: StateSet): Boolean = event match {
  case Event(_, Create, _, Some(_), _, _, prevEvents, _, _) => prevEvents.isEmpty
  case Event(_, _, _, stateKey, sender, _, prevEvents, _, _) =>
    isInRoom(sender, stateSet) && hasPowerLevel(
      sender,
      stateSetPowerLevels(stateSet),
      if (stateKey.isEmpty) 0 else 50
    )
  case _ => isAuthorized(event, augmentedStateSet(stateSet, event))
}

def addIfAuthorized(stateSet: StateSet, event: Event) =
  if (isAuthorized(event, stateSet)) insertEvent(event, stateSet) else stateSet

def iterativeAuthChecks(events: List[Event], stateSet: StateSet): StateSet = {
  events.foldLeft(stateSet)((acc, e) => addIfAuthorized(acc, e))
}

def isPowerEvent(event: Event): Boolean = event match {
  case Event(_, PowerLevels, _, Some(_), _, _, _, _, _)                  => true
  case Event(_, JoinRules, _, Some(_), _, _, _, _, _)                    => true
  case Event(_, Membership, _, Some(stateKey), sender, "ban", _, _, _)   => !(sender == stateKey)
  case Event(_, Membership, _, Some(stateKey), sender, "leave", _, _, _) => !(sender == stateKey)
  case _                                                                 => false
}

//val create = newStateEvent().copy(eventId = "create", eventType = Create, sender = "@alice:example.com")
val pl1    = setsPowerLevels("@alice:example.com", Map("@alice:example.com" -> 100)).copy(eventId = "power levels1")
val pl2 = setsPowerLevels("@alice:example.com", Map("@alice:example.com" -> 100, "@bob:example.com" -> 50))
  .copy(eventId = "power levels2")

val topic = setsTopic("@bob:example.com", "This is a topic").copy(eventId = "topic")
val stateSet1 = stateSetFromEventList(List(create, pl1))
val stateSet2 = stateSetFromEventList(List(create, pl2, topic))
val stateSet3 = stateSetFromEventList(List(create, pl2))
val stateSets = List(stateSet1, stateSet2, stateSet3)

val domain = Set.from(stateSets.flatMap(_.keys))

def eventsForKey(key: (EventType, String)) = Set.from(stateSets.map(_.get(key)))
val fullStateMapList = domain.toList.map(k => (k, eventsForKey(k)))
val (unconflictedList, conflictedList) = fullStateMapList.partition((k, events) => events.size == 1)