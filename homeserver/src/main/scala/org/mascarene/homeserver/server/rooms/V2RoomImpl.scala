/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.server.rooms

import java.util.UUID

import cats.data.Validated.{Invalid, Valid}
import com.typesafe.config.Config
import org.mascarene.homeserver.server.model.{
  AuthCache,
  DbContext,
  Event,
  EventRejection,
  EventTypes,
  Room,
  StateSetCache
}
import redis.clients.jedis.JedisPool
import org.mascarene.utils.{RoomIdentifierUtils, UserIdentifierUtils}
import cats.data.{Validated, ValidatedNec}
import cats.implicits._
import com.typesafe.scalalogging.LazyLogging
import io.circe.generic.auto._
import org.mascarene.matrix.client.r0.model.events.MemberEventContent

import scala.collection.immutable.VectorMap
import scala.collection.mutable
import scala.util.{Failure, Success, Try}

class V2RoomImpl(room: Room)(implicit config: Config, dbContext: DbContext, jedisPool: JedisPool)
    extends RoomVersionImpl
    with LazyLogging {

  type ValidationResult[A] = ValidatedNec[EventAuthValidation, A]

  private[this] val stateSetCache = new StateSetCache(config, dbContext, jedisPool)
  private[this] val authCache     = new AuthCache(config, dbContext, jedisPool)

  /**
    * Resolve event state according to https://matrix.uhoreg.ca/stateres/reloaded.html
    *
    * @param resolvedEvent event to resolve
    * @return
    */
  override def resolve(resolvedEvent: Event): Either[EventRejection, StateSet] =
    Try {
      val parentEvents = eventCache.getEventParentEdges(resolvedEvent.id)
      //State sets is composed of prev events state set + event state set if the event is a state event
      val eventStateSet: StateSet =
        if (resolvedEvent.isStateEvent)
          StateSet(Map(resolvedEvent.eventType -> Map(resolvedEvent.stateKey.get -> resolvedEvent)))
        else StateSet()
      val stateSets: Set[StateSet] = parentEvents.get
        .map(parent => stateSetCache.getStateSet(parent.id).get)
        .filter(_.nonEmpty)
        .map(_.get) + eventStateSet

      val (unconflictedStateMap, conflictedSet): (StateSet, Set[Event]) = calculateConflicts(stateSets)
      val fullConflictedSet: Set[Event]                                 = conflictedSet.union(authDifference(stateSets).get)
      val conflictedPowerEvents                                         = fullConflictedSet.filter(event => isPowerEvent(event).get)
      val conflictedPowerEventsWithAuth =
        conflictedPowerEvents.union(fullAuthChain(conflictedPowerEvents).get.intersect(fullConflictedSet))
      val sortedPowerEvents = revTopPowSort(conflictedPowerEventsWithAuth)
      val partialResolvedState: StateSet =
        iterativeAuthChecks(sortedPowerEvents.getOrElse(Seq.empty), unconflictedStateMap)
      val otherConflictedEvents: Set[Event] = fullConflictedSet.diff(conflictedPowerEventsWithAuth)
      val resolvedPowerLevel                = partialResolvedState.getEvent(EventTypes.M_ROOM_POWER_LEVELS, "")
      val sortedOtherEvents                 = resolvedPowerLevel.map { pl => otherConflictedEvents.toSeq.sorted(mainLineOrder(pl)) }
      val nearlyFinalState: StateSet        = iterativeAuthChecks(sortedOtherEvents.getOrElse(Seq.empty), partialResolvedState)
      val resolution: StateSet              = unconflictedStateMap ++ nearlyFinalState

      stateSetCache.put(resolvedEvent.id, resolution)
      resolution
    } match {
      case Success(stateSet) => Right(stateSet)
      case Failure(f)        => returnRejection(resolvedEvent.id, f.getMessage)
    }

  private def mainLineOrder(pl: Event): Ordering[Event] = {
    def mainLine(event: Event): Seq[Event] = {
      Seq.unfold[Event, Event](event)(e => findPL(event).map(plEvent => (e, plEvent)))
    }

    val mainlineMap: Map[Event, Int] = mainLine(pl).zip(LazyList.from(0, -1)).toMap

    def mainLineDepth(event: Event): Option[Int] = {
      val mayBeLd = mainLine(event).map(e => mainlineMap.get(e))
      if (mayBeLd.isEmpty)
        None
      else mayBeLd.head
    }

    (e1: Event, e2: Event) => {
      val comp1 = for {
        d1 <- mainLineDepth(e1)
        d2 <- mainLineDepth(e2)
      } yield d1 - d2
      comp1.getOrElse {
        val comp2 = e1.originServerTs.compareTo(e2.originServerTs)
        val comp3 = e1.mxEventId.compare(e2.mxEventId)
        if (comp2 == 0)
          comp3
        else
          comp2
      }
    }
  }

  def findPL(event: Event): Option[Event] =
    eventCache.getEventAuthEdges(event.id).get.find(_.eventType == EventTypes.M_ROOM_POWER_LEVELS)

  /**
    * Power events are defined as  a subset of state events which specifically have the power to add or remove
    * abilities from another user. These are:
    *  - Setting room power levels: m.room.power_levels where the state_key equals ""
    *  - Setting room join rules: m.room.join_rules where the state_key equals ""
    *  - Kicking a user: m.room.member with "leave" content but where the sender (the admin/mod) does not match
    * the state_key (the kicked user)
    *  - Banning a user: m.room.member with "ban" content but where the sender (the admin/mod) does not match the
    * state_key (the banned user)
    *
    * @param event
    * @return
    */
  private def isPowerEvent(event: Event): Try[Boolean] = Try {
    if (!event.isStateEvent)
      false
    else {
      event.eventType match {
        case EventTypes.M_ROOM_POWER_LEVELS if event.stateKey.contains("") => true
        case EventTypes.M_ROOM_JOIN_RULES if event.stateKey.contains("")   => true
        case EventTypes.M_ROOM_MEMBER =>
          val repoOps = for {
            sender        <- authCache.getUserById(event.senderId).get
            c             <- eventRepo.getEventContent(event).get
            memberContent <- c.content.get.as[MemberEventContent].toOption
          } yield (sender, memberContent)
          val (sender, content) = repoOps.get
          sender.mxUserId != event.stateKey.get && (content.membership == "ban" || content.membership == "leave")
        case _ => false
      }
    }
  }

  /**
    * Find the power level of an event sender
    *
    * @param event to look for event power level
    * @return sender power level
    */
  private def findSenderPowerLevel(event: Event): Int = {
    def findPowerLevelOrCreateEvent(events: Set[Event]): Option[Event] = {
      events
        .find(e => e.eventType == EventTypes.M_ROOM_POWER_LEVELS)
        .orElse(events.find(e => e.eventType == EventTypes.M_ROOM_CREATE))
    }

    val res = for {
      sender     <- authCache.getUserById(event.senderId)
      plOrCreate <- eventCache.getEventAuthEdges(event.id).map(findPowerLevelOrCreateEvent)
      content    <- eventRepo.getEventContent(event)
    } yield (sender, plOrCreate, content)
    res.map {
      case (Some(user), Some(plOrCreate), content) =>
        plOrCreate.eventType match {
          case EventTypes.M_ROOM_CREATE => if (event.senderId == plOrCreate.senderId) 100 else 0
          case EventTypes.M_ROOM_POWER_LEVELS =>
            val plContent = PowerLevelsUtils.getPlContent(content.get)
            PowerLevelsUtils.findUserPowerLevel(user.mxUserId, plContent)
        }
      case _ => 0
    }
  } match {
    case Success(pl) => pl
    case Failure(f) =>
      logger.warn(s"Failed to get user power level for event ${event.id}. Cause: ${f.getMessage}")
      logger.debug("details", f)
      0
  }

  /**
    * Reverse ordering sort method
    * The reverse topological power ordering of a set of events is the lexicographically smallest topological
    * ordering based on the DAG formed by auth events. The reverse topological power ordering is ordered from
    * earliest event to latest. For comparing two topological orderings to determine which is the lexicographically
    * smallest, the following comparison relation on events is used: for events x and y, x < y if
    *  - x's sender has greater power level than y's sender, when looking at their respective auth_events; or
    *  - the senders have the same power level, but x's origin_server_ts is less than y's origin_server_ts; or
    *  - the senders have the same power level and the events have the same origin_server_ts, but x's event_id is
    * less than y's event_id.
    *
    * @param eventMap
    * @param incomingEdges
    * @return
    */
  private def sortedIncomingEdges(eventMap: Map[UUID, Event], incomingEdges: Map[UUID, Int]): VectorMap[UUID, Int] = {
    //logger.debug(s"incomingEdges: $incomingEdges, eventMap: $eventMap")
    val eventOrdering = new Ordering[Event] {
      override def compare(x: Event, y: Event): Int = {
        val comp1 = findSenderPowerLevel(x) - findSenderPowerLevel(y)
        val comp2 = x.originServerTs.compareTo(y.originServerTs)
        val comp3 = x.mxEventId.compare(y.mxEventId)
        if (comp1 != 0)
          comp1
        else {
          if (comp2 != 0)
            comp2
          else
            comp3
        }
      }
    }

    val outgoing = VectorMap() ++
      eventMap
        .filter {
          case (uuid, _) => incomingEdges.contains(uuid)
        }
        .values
        .toSeq
        .sorted(eventOrdering)
        .map(ev => ev.id -> incomingEdges(ev.id))
    //logger.debug(s"outgoing sorted edges: $outgoing")
    outgoing
  }

  /**
    * Reverse topological ordering of control events according to :
    *  - https://matrix.org/docs/guides/implementing-stateres
    *  - https://matrix.org/docs/spec/rooms/v2
    *
    * @param powerEvents list of power events to sort
    * @return list of sorted events
    */
  private def revTopPowSort(powerEvents: Set[Event]): Try[Seq[Event]] = Try {
    val incomingEdges: mutable.Map[UUID, Int] = mutable.Map.from(powerEvents.map(e => e.id -> 0))

    val powerEventsChain = powerEvents
      .flatMap { event => authChain(event).get }
    incomingEdges ++= powerEventsChain.groupMapReduce(_.id)(_ => 1)(_ + _)

    val eventsMap: Map[UUID, Event] = Map.from((powerEvents ++ powerEventsChain).map(e => e.id -> e))

    var outputEvents = mutable.Seq.empty[Event]
    while (incomingEdges.nonEmpty) {
      sortedIncomingEdges(eventsMap, incomingEdges.toMap).foreach {
        case (eventId, edgeCount) =>
          if (edgeCount == 0)
            outputEvents = outputEvents.prepended(eventsMap(eventId))
          eventCache.getEventAuthEdges(eventId).map {
            _.map { authEvent => incomingEdges.update(authEvent.id, incomingEdges(authEvent.id) - 1) }
          }
          incomingEdges.remove(eventId)
      }
    }
    outputEvents.toSeq
  }

  private def calculateConflicts(stateSets: Set[StateSet]): (StateSet, Set[Event]) = {
    def eventsForKey(key: (String, String), stateSets: Set[StateSet]) =
      stateSets.map(stateSet => stateSet.getEvent(key)).filter(_.isDefined).map(_.get)

    // Extract unique (eventType, stateKey) from a set of state set
    val domain                             = stateSets.flatMap(_.keys)
    val fullStateMapList                   = domain.map(k => k -> eventsForKey(k, stateSets))
    val (unconflictedList, conflictedList) = fullStateMapList.partition { case (k, events) => events.size == 1 }
    val unconflictedStateMap: StateSet = unconflictedList
      .map { case (k, eventSet) => eventSet.head }
      .foldLeft(StateSet())((stateSet, event) => stateSet.updatedWith(event)) //updateStateSet(stateSet, event))
    val conflictedSet = Set.concat(conflictedList.flatMap(_._2))
    (unconflictedStateMap, conflictedSet)
  }

  private def fullAuthChain(events: Set[Event]): Try[Set[Event]] = Try {
    Set.from(events.flatMap(e => authChain(e).get))
  }

  /**
    * Given some state sets, the auth difference is calculated by first calculating the full auth chain for each
    * state set (that is, the union of the auth chains for the events in the state set) and taking every event that
    * doesn't appear in every auth chain.
    * In other words, if we take the full auth chains for the state sets, then the auth difference is their union
    * minus their intersection.
    *
    * @param stateSets
    * @return
    */
  private def authDifference(stateSets: Set[StateSet]) = Try {
    val fullAuthChains =
      stateSets
        .map(stateSet => stateSet.values)
        .map(events => fullAuthChain(Set.from(events)).get)
    val authChainsUnion: Set[Event] = Set.concat(fullAuthChains.flatten)
    val authChainsIntersection: Set[Event] =
      if (fullAuthChains.isEmpty) Set.empty else fullAuthChains.reduceLeft((acc, set) => acc.intersect(set))
    authChainsUnion.diff(authChainsIntersection)
  }

  private def returnRejection(eventId: UUID, cause: String) = Left(
    eventRepo
      .addEventRejection(eventId, Some(cause))
      .getOrElse(EventRejection(eventId, Some("Internal repository error while inserting event rejection")))
  )

  private def authChain(event: Event): Try[Set[Event]] = {
    for {
      authEvents <- eventCache.getEventAuthEdges(event.id)
      r <- Try {
        authEvents.flatMap(e => authChain(e).get)
      }
    } yield authEvents.union(r)
  }

  def iterativeAuthChecks(events: Seq[Event], stateSet: StateSet): StateSet = {
    events.foldLeft(stateSet) { (stateSetAcc, event) =>
      if (isAuthorized(event, stateSet)) {
        stateSetAcc.updatedWith(event)
      } else stateSetAcc
    }
  }

  def isAuthorized(e: Event, stateSet: StateSet): Boolean = {
    val augmentedStateSet = eventCache.getEventAuthEdges(e.id).get.foldLeft(stateSet) { (stateSetAcc, e) =>
      val (eventType, stateKey) = (e.eventType, e.stateKey.get)
      if (stateSetAcc.exists(eventType, stateKey)) stateSetAcc else stateSetAcc.updatedWith(e)
    }
    checkAuthorization(e, augmentedStateSet) match {
      case Right(_) => true
      case _        => false
    }
  }

  /**
    * Event authorization checks according to https://matrix.org/docs/spec/rooms/v1#authorization-rules
    *
    * @param event    event to check authorizations
    * @param stateSet stateSet containing state events for this event state
    */
  private def checkAuthorization(event: Event, stateSet: StateSet): Either[EventRejection, Unit] = {
    val validationResult: ValidationResult[Event] = event.eventType match {
      case EventTypes.M_ROOM_CREATE  => validateMRoomCreateEvent(event, stateSet)
      case EventTypes.M_ROOM_ALIASES => validateMRoomAliasesEvent(event, stateSet)
      case EventTypes.M_ROOM_MEMBER  => validateMRoomMemberEvent(event, stateSet)
      case _                         => Valid(event)
    }
    validationResult match {
      case Valid(_)       => Right(())
      case Invalid(chain) => Left(chain.head.rejection)
    }
  }

  /**
    * Is the event sender in the room ?
    * We answer by looking for m.room.membership event sent by this user with "join" membership
    *
    * @param event
    * @param stateSet
    * @return
    */
  private def isInRoom(event: Event, stateSet: StateSet): Boolean = {
    val mayBeMemberEventContent = for {
      sender             <- authCache.getUserById(event.senderId).get //get event sender
      memberEvent        <- stateSet.getEvent(EventTypes.M_ROOM_MEMBER, sender.mxUserId) // get membership event for this user
      content            <- eventRepo.getEventContent(memberEvent).get // check content
      memberEventContent <- content.content.flatMap(_.as[MemberEventContent].toOption)
    } yield memberEventContent
    mayBeMemberEventContent match {
      case Some(memberEventContent) => memberEventContent.membership == "join"
      case _                        => false
    }
  }

  private def validateAuthEvents(e: Event, stateSet: StateSet): ValidationResult[Event] = {
    authChain(e)
      .map { authEvents =>
        val duplicates = authEvents.groupBy(event => (event.eventType, event.stateKey.get)).count(_._2.size > 1)
        if (duplicates > 0)
          DuplicateEntriesInAuthEvents(e).invalidNec
        else {
          authEvents.find(event => event.eventType == EventTypes.M_ROOM_CREATE) match {
            case Some(_) => e.validNec
            case None    => NoRoomCreateEventInAuthChain(e).invalidNec
          }
        }

      }
      .recover(f => InternalError(e, f).invalidNec)
      .get
  }

  private def validateMRoomMemberEvent(event: Event, stateSet: StateSet): ValidationResult[Event] = {
    def validateJoinMemberShip(event: Event, content: MemberEventContent): ValidationResult[Event] = {
      validatePreviousEvent(event).andThen(validateSender(stateSet))
    }

    def validatePreviousEvent(event: Event): ValidationResult[Event] = {
      eventCache
        .getEventParentEdges(event.id)
        .map { parents =>
          if (parents.head.eventType == EventTypes.M_ROOM_MEMBER && parents.size == 1)
            event.validNec
          else ParentEventMustBeUniqueAndCreate(event).invalidNec
        }
        .recover(f => InternalError(event, f).invalidNec)
        .get
    }

    def validateSender(stateSet: StateSet)(event: Event): ValidationResult[Event] = {
      authCache
        .getUserById(event.senderId)
        .map { sender =>
          (sender, event.stateKey) match {
            case (Some(sender), Some(stateKey)) if sender.mxUserId == stateKey => event.validNec
            case _                                                             => SenderMismatchStatekey(event).invalidNec
          }
        }
        .recover(f => InternalError(event, f).invalidNec)
        .get
      //TODO check if user is not ban
    }

    validateAuthEvents(event, stateSet).andThen { event =>
      val tryContent = for {
        eventContent <- eventRepo.getEventContent(event)
        content      <- eventContent.get.content.get.as[MemberEventContent].toTry
      } yield content
      tryContent
        .map { membershipContent =>
          membershipContent.membership match {
            case "join"   => validateJoinMemberShip(event, membershipContent)
            case "invite" => Valid(event)
            case "invite" => Valid(event)
            case "leave"  => Valid(event)
            case "ban"    => Valid(event)
          }
        }
        .recover(f => InvalidContent(event).invalidNec)
        .get
    }
  }

  private def validateMRoomAliasesEvent(event: Event, stateSet: StateSet): ValidationResult[Event] = {
    def validateSenderDomainMatchesStateKey(event: Event): ValidationResult[Event] = {
      val domainMatch = for {
        sender         <- authCache.getUserById(event.senderId)
        userIdentifier <- UserIdentifierUtils.parse(sender.get.mxUserId)
        res            <- Try { userIdentifier.domain == event.stateKey.get }
      } yield res
      domainMatch match {
        case Failure(f)     => InvalidIdentifierFormat(f.getMessage, event).invalidNec
        case Success(false) => SenderDomainMismatchStateKey(event).invalidNec
        case Success(true)  => event.validNec
      }
    }
    validateAuthEvents(event, stateSet).andThen(validateSenderDomainMatchesStateKey)
  }

  private def validateMRoomCreateEvent(e: Event, stateSet: StateSet): ValidationResult[Event] = {
    def validateMRoomCreateEventHasNoParent(event: Event): ValidationResult[Event] = {
      eventCache
        .getEventParentEdges(event.id)
        .map { parents =>
          if (parents.nonEmpty)
            MRoomCreateHasNoParent(event, parents).invalidNec
          else
            event.validNec
        }
        .recover(f => InternalError(e, f).invalidNec)
        .get
    }

    def validateRoomIdDomainMatchesSenderDomain(event: Event): ValidationResult[Event] = {
      val domainMatch = for {
        sender         <- authCache.getUserById(event.senderId)
        userIdentifier <- UserIdentifierUtils.parse(sender.get.mxUserId)
        roomIdentifier <- RoomIdentifierUtils.parse(room.mxRoomId)
      } yield userIdentifier.domain == roomIdentifier.domain
      domainMatch match {
        case Failure(f)     => InvalidIdentifierFormat(f.getMessage, event).invalidNec
        case Success(false) => RoomDomainMismatchSenderDomain(event).invalidNec
        case Success(true)  => event.validNec
      }
    }
    validateMRoomCreateEventHasNoParent(e).andThen(validateRoomIdDomainMatchesSenderDomain)
  }

}

object V2RoomImpl {
  def apply(room: Room)(implicit config: Config, dbContext: DbContext, jedisPool: JedisPool) =
    new V2RoomImpl(room)
}
