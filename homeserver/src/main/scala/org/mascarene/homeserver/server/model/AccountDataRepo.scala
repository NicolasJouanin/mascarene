/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.server.model

import java.time.LocalDateTime
import java.util.UUID

import scala.util.Try

case class AccountData(
    id: UUID,
    userId: UUID,
    eventType: String,
    eventContent: String,
    createdAt: LocalDateTime,
    updatedAt: Option[LocalDateTime]
)

class AccountDataRepo(dbContext: DbContext) {
  import dbContext._

  private val accountDatas = quote(querySchema[AccountData]("account_data"))

  def storeAccountData(userId: UUID, eventType: String, eventContent: String): Try[AccountData] = Try {
    val newRow = AccountData(UUID.randomUUID(), userId, eventType, eventContent, LocalDateTime.now(), None)
    run {
      accountDatas.insert(lift(newRow))
    }
    newRow
  }

  def getAccountData(userId: UUID, eventType: String): Try[Option[AccountData]] = Try {
    run(accountDatas.filter(_.userId == lift(userId)).filter(_.eventType == lift(eventType))).headOption
  }
}
