/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.server.rooms

import java.util.UUID

import akka.actor.typed.scaladsl.AskPattern._
import akka.actor.typed.{ActorRef, ActorSystem}
import akka.cluster.sharding.typed.scaladsl.EntityRef
import com.typesafe.config.Config
import com.typesafe.scalalogging.LazyLogging
import io.circe.generic.auto._
import io.circe.syntax._
import org.mascarene.homeserver.ImplicitAskTimeOut
import org.mascarene.homeserver.server.model._
import org.mascarene.homeserver.server.rooms.RoomServer.{PostEvent, RoomEvent}
import org.mascarene.matrix.client.r0.model.events.MemberEventContent
import org.mascarene.matrix.client.r0.model.rooms.{
  CreateRoomRequest,
  CreationContent,
  PowerLevelEventContent,
  StateEvent
}
import org.mascarene.sdk.matrix.core.ApiFailure
import org.mascarene.utils.RoomIdentifierUtils

import scala.concurrent.Future
import scala.util.{Failure, Success, Try}

class RoomApi(
    val config: Config,
    dbContext: DbContext,
    roomCluster: ActorRef[RoomCluster.Command],
    implicit val system: ActorSystem[_]
) extends LazyLogging
    with ImplicitAskTimeOut {

  implicit private val ec           = system.executionContext
  private val matrixServerName      = config.getString("mascarene.server.domain-name")
  private val defaultRoomVersion    = config.getString("mascarene.matrix.default-room-version")
  private val supportedRoomVersions = config.getStringList("mascarene.matrix.supported-room-versions")

  private val roomRepo = new RoomRepo(dbContext)

  def createRoom(visibility: String, version: Option[String] = Some(defaultRoomVersion)): Try[Room] = {
    for {
      validatedVersion <- version match {
        case Some(v) if supportedRoomVersions.contains(v) => Success(v)
        case None                                         => Success(defaultRoomVersion)
        case _                                            => Failure(new ApiFailure("M_UNSUPPORTED_ROOM_VERSION"))
      }
      room <- roomRepo.createRoom(RoomIdentifierUtils.generate(matrixServerName).toString, visibility, validatedVersion)
    } yield room
  }

  def getRoomByMxId(mxId: String): Try[Option[Room]] = roomRepo.getRoomByMxId(mxId)

  def getRoomServer(room: Room): Future[EntityRef[RoomServer.Command]] = {
    roomCluster.ask[RoomCluster.GotRoomServer](RoomCluster.GetRoomServer(room, _)).map(_.serverRef)
  }

  /**
    * Create all events necessary for a room creation
    * @param roomServer RoomServer instance in charge of handling events
    * @param sender User sender of the creation request
    * @param request API creation request
    * @return
    */
  def postCreateRoomEvents(
      roomServer: EntityRef[RoomServer.Command],
      sender: User,
      request: CreateRoomRequest
  ): Future[RoomState] = {
    import RoomServer._
    val creationContent =
      request.creation_content
        .getOrElse(CreationContent(creator = sender.mxUserId))
        .copy(creator = sender.mxUserId, room_version = Some(defaultRoomVersion))
    val defaultPowerLevels = PowerLevelEventContent(
      ban = 50,
      events = Map.empty,
      events_default = 0,
      invite = 50,
      kick = 50,
      redact = 50,
      state_default = 50,
      Map(sender.mxUserId -> 100),
      users_default = Some(0),
      notifications = Map.empty
    )
    val powerLevels = request.power_level_content_override match {
      case Some(powerOverride) =>
        defaultPowerLevels.copy(
          ban = powerOverride.ban,
          events = defaultPowerLevels.events ++ powerOverride.events,
          events_default = powerOverride.events_default,
          invite = powerOverride.invite,
          kick = powerOverride.kick,
          redact = powerOverride.redact,
          state_default = powerOverride.state_default,
          users = defaultPowerLevels.users ++ powerOverride.users,
          users_default = powerOverride.users_default,
          notifications = defaultPowerLevels.notifications ++ powerOverride.notifications
        )
      case None => defaultPowerLevels
    }

    //TODO: implement invitation events on room creation
    if (request.invite.isDefined && request.invite.get.nonEmpty)
      logger.warn("Invitations not yet supported on room creation")
    if (request.invite_3pid.isDefined && request.invite_3pid.get.nonEmpty)
      logger.warn("3Pid invitations not yet supported on room creation")

    for {
      createEvent <- roomServer
        .ask[RoomEvent](replyTo =>
          PostEvent(
            eventType = EventTypes.M_ROOM_CREATE,
            stateKey = Some(""),
            senderId = sender.userId,
            content = Some(creationContent.asJson),
            replyTo = Some(replyTo)
          )
        )
        .map(_.event)
      memberEvent <- roomServer
        .ask[RoomEvent](replyTo =>
          PostEvent(
            eventType = EventTypes.M_ROOM_MEMBER,
            stateKey = Some(sender.mxUserId),
            senderId = sender.userId,
            content = Some(MemberEventContent(membership = "join", is_direct = request.is_direct).asJson),
            parentsId = Set(createEvent.get.id),
            authsEventId = Set(createEvent.get.id),
            replyTo = Some(replyTo)
          )
        )
        .map(_.event)
      powerLevelsEvent <- roomServer
        .ask[RoomEvent](replyTo =>
          PostEvent(
            eventType = EventTypes.M_ROOM_POWER_LEVELS,
            stateKey = Some(""),
            senderId = sender.userId,
            content = Some(powerLevels.asJson),
            parentsId = Set(memberEvent.get.id),
            authsEventId = Set(createEvent.get.id),
            replyTo = Some(replyTo)
          )
        )
        .map(_.event)
      (joinRulesEvent, historyVisibilityEvent, guestAccessEvent) <- createPresetEvents(
        request.preset,
        sender,
        powerLevelsEvent.get,
        Set(memberEvent.get.id, powerLevelsEvent.get.id),
        roomServer
      )
      stateEvents <- createInitialStateEvents(
        request.initial_state.getOrElse(List.empty),
        sender,
        guestAccessEvent,
        Set(memberEvent.get.id, powerLevelsEvent.get.id),
        roomServer
      )
      _ <- createNameAndTopic(
        request.name,
        request.topic,
        sender,
        stateEvents.appended(guestAccessEvent).last, //get last event
        Set(memberEvent.get.id, powerLevelsEvent.get.id),
        roomServer
      )
      roomStateResponse <- roomServer.ask[RoomServer.RoomStateResponse](replyTo => RoomServer.GetRoomState(replyTo))
    } yield roomStateResponse.roomState
  }

  private def createPresetEvents(
      preset: Option[String],
      sender: User,
      lastEVent: Event,
      authEvents: Set[UUID],
      roomServer: EntityRef[RoomServer.Command]
  ): Future[(Event, Event, Event)] = {
    val (joinRule, historyVisibility, guestAccess): (String, String, String) = preset match {
      case Some("private_chat")         => ("invite", "shared", "can_join")
      case Some("trusted_private_chat") => ("invite", "shared", "can_join")
      case Some("public_chat")          => ("public", "shared", "forbidden")
      case other                        => throw new ApiFailure("M_UNRECOGNIZED", s"Unrecognized preset value '$other'")
    }
    for {
      joinRulesEvent <- roomServer
        .ask[RoomEvent](replyTo =>
          PostEvent(
            eventType = EventTypes.M_ROOM_JOIN_RULES,
            stateKey = Some(""),
            senderId = sender.userId,
            content = Some(Map("join_rule" -> joinRule).asJson),
            replyTo = Some(replyTo),
            parentsId = Set(lastEVent.id),
            authsEventId = authEvents
          )
        )
        .map(_.event)
      historyVisibilityEvent <- roomServer
        .ask[RoomEvent](replyTo =>
          PostEvent(
            eventType = EventTypes.M_ROOM_HISTORY_VISIBILITY,
            stateKey = Some(""),
            senderId = sender.userId,
            content = Some(Map("history_visibility" -> historyVisibility).asJson),
            replyTo = Some(replyTo),
            parentsId = Set(joinRulesEvent.get.id),
            authsEventId = authEvents
          )
        )
        .map(_.event)
      guestAccessEvent <- roomServer
        .ask[RoomEvent](replyTo =>
          PostEvent(
            eventType = EventTypes.M_ROOM_HISTORY_VISIBILITY,
            stateKey = Some(""),
            senderId = sender.userId,
            content = Some(Map("guest_access" -> guestAccess).asJson),
            replyTo = Some(replyTo),
            parentsId = Set(historyVisibilityEvent.get.id),
            authsEventId = authEvents
          )
        )
        .map(_.event)
    } yield (joinRulesEvent.get, historyVisibilityEvent.get, guestAccessEvent.get)
  }

  private def createInitialStateEvents(
      initialState: List[StateEvent],
      sender: User,
      lastEVent: Event,
      authEvents: Set[UUID],
      roomServer: EntityRef[RoomServer.Command]
  ): Future[List[Event]] = {
    Future.sequence {
      initialState.map { stateEvent =>
        roomServer
          .ask[RoomEvent](replyTo =>
            PostEvent(
              eventType = stateEvent.`type`,
              stateKey = Some(stateEvent.state_key),
              senderId = sender.userId,
              content = stateEvent.content,
              replyTo = Some(replyTo),
              parentsId = Set(lastEVent.id),
              authsEventId = authEvents
            )
          )
          .map(_.event.get)
      }
    }
  }

  private def createNameAndTopic(
      nameOpt: Option[String],
      topicOpt: Option[String],
      sender: User,
      lastEVent: Event,
      authEvents: Set[UUID],
      roomServer: EntityRef[RoomServer.Command]
  ): Future[(Option[Event], Option[Event])] = {
    val nameFuture = nameOpt.map { name =>
      roomServer
        .ask[RoomEvent](replyTo =>
          PostEvent(
            eventType = EventTypes.M_ROOM_NAME,
            stateKey = Some(""),
            senderId = sender.userId,
            content = Some(Map("name" -> name).asJson),
            replyTo = Some(replyTo),
            parentsId = Set(lastEVent.id),
            authsEventId = authEvents
          )
        )
        .map(_.event.get)
    }
    val topicFuture = topicOpt.map { topic =>
      roomServer
        .ask[RoomEvent](replyTo =>
          PostEvent(
            eventType = EventTypes.M_ROOM_TOPIC,
            stateKey = Some(""),
            senderId = sender.userId,
            content = Some(Map("topic" -> topic).asJson),
            replyTo = Some(replyTo),
            parentsId = Set(lastEVent.id),
            authsEventId = authEvents
          )
        )
        .map(_.event.get)
    }

    (nameFuture, topicFuture) match {
      case (None, None)         => Future.successful((None, None))
      case (Some(f1), None)     => f1.map(e => (Some(e), None))
      case (None, Some(f2))     => f2.map(e => (None, Some(e)))
      case (Some(f1), Some(f2)) => f1.flatMap(e1 => f2.map(e2 => (Some(e1), Some(e2))))
    }
  }
}
