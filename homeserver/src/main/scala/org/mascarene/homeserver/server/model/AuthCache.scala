/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.server.model

import java.util.UUID

import com.typesafe.config.Config
import com.typesafe.scalalogging.LazyLogging
import redis.clients.jedis.JedisPool
import scalacache._
import scalacache.redis._
import scalacache.modes.try_._
import io.circe.generic.auto._
import scalacache.serialization.circe._

import scala.jdk.DurationConverters._
import scala.util.{Success, Try}

class AuthCache(config: Config, dbContext: DbContext, jedisPool: JedisPool) extends LazyLogging {
  private[this] val authRepo                = new AuthRepo(dbContext)
  private[this] val usersCache: Cache[User] = RedisCache(jedisPool)

  private val userCacheTtl = Some(config.getDuration("mascarene.server.cache-ttl.user").toScala)
  logger.debug(s"user cache TTL=$userCacheTtl")

  private def userKey(userId: UUID) = s"user($userId)"

  def getUserById(userId: UUID): Try[Option[User]] = {
    usersCache.get(userKey(userId)).flatMap {
      case Some(user) =>
        Success(Some(user))
      case None =>
        authRepo.getUserById(userId).map { maybeUser =>
          maybeUser.map { user =>
            usersCache.put(userKey(userId))(user, userCacheTtl)
            user
          }
        }
    }
  }
}
