/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.server.model

import java.sql.PreparedStatement
import java.time.LocalDateTime
import java.util.UUID

import io.circe
import io.circe._
import io.circe.parser._
import io.circe.syntax._
import io.circe.generic.auto._
import org.mascarene.matrix.client.r0.model.events.UnsignedData

import scala.util.Try

case class Event(
    id: UUID,
    mxEventId: Option[String],
    stateKey: Option[String],
    originServerTs: LocalDateTime,
    receivedTs: Option[LocalDateTime],
    senderId: UUID,
    roomId: UUID,
    eventType: String,
    unsigned: Option[UnsignedData],
    createdAt: LocalDateTime
) {
  def isStateEvent: Boolean = stateKey.isDefined
}

case class EventAuthEdge(eventId: UUID, authEventId: UUID)
case class EventParentEdge(eventId: UUID, parentEventId: UUID)
case class EventRejection(eventId: UUID, cause: Option[String])
case class EventContent(eventId: UUID, content: Option[Json] = None)

class EventRepo(dbContext: DbContext) {
  import dbContext._

  private val events           = quote(querySchema[Event]("events"))
  private val eventAuthsEdges  = quote(querySchema[EventAuthEdge]("event_auth_edges"))
  private val eventParentEdges = quote(querySchema[EventParentEdge]("event_parent_edges"))
  private val eventRejections  = quote(querySchema[EventRejection]("event_rejections"))
  private val eventContents    = quote(querySchema[EventContent]("event_contents"))

  private implicit val jsonDecoder: Decoder[Json] =
    decoder((index, row) => parse(row.getObject(index).toString).fold(error => throw error, json => json))
  private implicit val jsonEncoder: Encoder[Json] = encoder[Json](
    java.sql.Types.OTHER,
    (index: Int, value: Json, row: PreparedStatement) =>
      row.setObject(index, value.asJson.noSpaces, java.sql.Types.OTHER)
  )

  private implicit val udDecoder: Decoder[UnsignedData] =
    decoder((index, row) => decode[UnsignedData](row.getObject(index).toString).fold(error => throw error, fd => fd))

  private implicit val udEncoder: Encoder[UnsignedData] = encoder[UnsignedData](
    java.sql.Types.OTHER,
    (index: Int, value: UnsignedData, row: PreparedStatement) =>
      row.setObject(index, value.asJson.noSpaces, java.sql.Types.OTHER)
  )

  def getEventsByMxIds(mxIds: Set[String]): Try[Set[Event]] = Try {
    run { events.filter(e => liftQuery(mxIds).contains(e.mxEventId)) }.toSet
  }

  def getEventsByIds(ids: Set[UUID]): Try[Set[Event]] = Try {
    run { events.filter(e => liftQuery(ids).contains(e.id)) }.toSet
  }

  def getEventById(eventId: UUID): Try[Option[Event]] = Try {
    run(events.filter(_.id == lift(eventId))).headOption
  }

  def insertEvent(
      roomId: UUID,
      senderId: UUID,
      eventType: String,
      stateKey: Option[String] = None,
      content: Option[Json] = None,
      parentsId: Set[UUID] = Set.empty,
      authsEventId: Set[UUID] = Set.empty,
      rejected: Boolean = false,
      rejectionCause: Option[String] = None,
      mxEventId: Option[String] = None,
      unsignedData: Option[UnsignedData] = None,
      originServerTs: LocalDateTime,
      receivedTs: Option[LocalDateTime] = None
  ): Try[Event] = {
    val newEvent = Event(
      UUID.randomUUID(),
      mxEventId,
      stateKey,
      originServerTs,
      receivedTs,
      senderId,
      roomId,
      eventType,
      unsignedData,
      LocalDateTime.now()
    )

    transaction[Try[Event]] {
      for {
        event <- Try {
          run(events.insert(lift(newEvent)))
          newEvent
        }
        eventContent <- insertEventContent(newEvent, content)
        parentEdges  <- insertParentEdgesAssoc(newEvent, parentsId)
        authEdges    <- insertAuthEdgesAssoc(newEvent, authsEventId)
        _ <- Try {
          if (rejected) run(eventRejections.insert(lift(EventRejection(newEvent.id, rejectionCause)))) else 0
        }
      } yield event
    }
  }

  /**
    * Insert an event content
    * @param event event for which the content is created
    * @param content event content
    * @return instance of EventContent created
    */
  def insertEventContent(event: Event, content: Option[Json]): Try[EventContent] = Try {
    val newEventContent = EventContent(event.id, content)
    run { eventContents.insert(lift(newEventContent)) }
    newEventContent
  }

  /**
    * Create associations between an event and its parents.
    * @param event event to associate
    * @param parentsId Set of matrixId or UUID to associate (UUID may not exist if parent is not none)
    * @return number of inserted rows
    */
  def insertParentEdgesAssoc(event: Event, parentsId: Set[UUID]): Try[Set[EventParentEdge]] = Try {
    val edges = parentsId.map(parentId => EventParentEdge(event.id, parentId))
    run {
      quote {
        liftQuery(edges).foreach { edge => eventParentEdges.insert(edge) }
      }
    }
    edges
  }

  /**
    * Create associations between an event and its auth events.
    * @param event event to associate
    * @param authEventIds Map of matrixId or UUID to associate (UUID may not exist if auth event is not none)
    * @return number of inserted rows
    */
  def insertAuthEdgesAssoc(event: Event, authEventIds: Set[UUID]): Try[Set[EventAuthEdge]] = Try {
    val edges = authEventIds.map(authEventId => EventAuthEdge(event.id, authEventId))
    run {
      quote {
        liftQuery(edges).foreach { edge => eventAuthsEdges.insert(edge) }
      }
    }
    edges
  }

  /**
    * Update an event by setting if matrixId
    * @param eventId ID of the event to update
    * @param mxEventId matrixID to set
    * @return number of updated rows (should be 0 or 1)
    */
  def updateEventMxId(eventId: UUID, mxEventId: Option[String]): Try[Long] = Try {
    run(events.filter(_.id == lift(eventId)).update(_.mxEventId -> lift(mxEventId)))
  }

  def addEventRejection(eventId: UUID, rejectionCause: Option[String]): Try[EventRejection] = Try {
    val eventRejection = EventRejection(eventId, rejectionCause)
    run { eventRejections.insert(lift(eventRejection)) }
    eventRejection
  }

  /**
    * get set of events which auth the given event
    * @param event
    * @return
    */
  def getAuthEvents(eventId: UUID): Try[Set[Event]] = Try {
    run {
      eventAuthsEdges.filter(_.eventId == lift(eventId)).join(events).on(_.authEventId == _.id).map {
        case (_, authEvent) => authEvent
      }
    }.toSet
  }

  /**
    * Get set of an event's parents
    * @param event
    * @return
    */
  def getParentEvents(eventId: UUID): Try[Set[Event]] = Try {
    run {
      eventParentEdges.filter(_.eventId == lift(eventId)).join(events).on(_.parentEventId == _.id).map {
        case (_, authEvent) => authEvent
      }
    }.toSet
  }

  def getEventContent(event: Event): Try[Option[EventContent]] = Try {
    run {
      eventContents.filter(_.eventId == lift(event.id))
    }.headOption
  }
}
