/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.server.rooms

import java.time.LocalDateTime
import java.util.UUID

import akka.actor.typed.scaladsl.AbstractBehavior
import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import akka.actor.typed.{ActorRef, Behavior}
import akka.cluster.sharding.typed.scaladsl.EntityTypeKey
import akka.util.Timeout

import scala.concurrent.duration._
import akka.util.Timeout
import com.typesafe.config.Config
import com.typesafe.scalalogging.Logger
import io.circe.Json
import io.circe.generic.auto._
import org.mascarene.homeserver.ImplicitAskTimeOut
import org.mascarene.homeserver.server.model._
import org.mascarene.homeserver.server.rooms.RoomWorker.{
  ResolutionRequest,
  ResolveAck,
  ResolveStreamCompleted,
  ResolveStreamFailure,
  ResolveStreamInit
}
import org.mascarene.sdk.matrix.core.ApiFailure
import org.mascarene.utils.Codecs
import org.mascarene.utils.LogUtils.time
import redis.clients.jedis.JedisPool
import scalacache._
import scalacache.redis._
import scalacache.serialization.circe._
import akka.actor.typed.scaladsl.adapter._
import akka.stream.{OverflowStrategy, QueueOfferResult}
import akka.stream.scaladsl.Source
import akka.stream.typed.scaladsl.ActorSink

import scala.util.{Failure, Success, Try}

object RoomServer {
  val TypeKey: EntityTypeKey[RoomServer.Command] = EntityTypeKey[RoomServer.Command]("RoomServer")
  sealed trait Response
  case class RoomStateResponse(roomState: RoomState) extends Response
  case class RoomEvent(event: Try[Event])            extends Response

  sealed trait Command
  case class InitialState(room: Room)                           extends Command
  case class GetRoomState(replyTo: ActorRef[RoomStateResponse]) extends Command
  case class SetVisibility(visibility: String)                  extends Command
  case class PostEvent(
      eventType: String,
      stateKey: Option[String],
      senderId: UUID,
      content: Option[Json],
      parentsId: Set[UUID] = Set.empty,
      authsEventId: Set[UUID] = Set.empty,
      replyTo: Option[ActorRef[RoomEvent]]
  ) extends Command
  case class WrappedQueueOfferResult(response: QueueOfferResult) extends Command
  case class WrappedQueueOfferFailure(ex: Throwable)             extends Command

  def apply(
      entityId: String,
      config: Config,
      dbContext: DbContext,
      jedisPool: JedisPool,
      workerRouter: ActorRef[RoomWorker.Command]
  ): Behavior[RoomServer.Command] =
    Behaviors.setup { ctx => new RoomServer(entityId, config, ctx, dbContext, jedisPool, workerRouter) }
}

class RoomServer(
    roomId: String,
    val config: Config,
    context: ActorContext[RoomServer.Command],
    dbContext: DbContext,
    jedisPool: JedisPool,
    workerRouter: ActorRef[RoomWorker.Command]
) extends AbstractBehavior[RoomServer.Command](context)
    with ImplicitAskTimeOut {
  import RoomServer._
  private val roomRepo                 = new RoomRepo(dbContext)
  private val eventRepo                = new EventRepo(dbContext)
  private val eventProcessStageRepo    = new EventProcessStageRepo(dbContext)
  private val eventCache: Cache[Event] = RedisCache(jedisPool)

  private[this] val matrixServerName = config.getString("mascarene.server.domain-name")
  private[this] val room: Room = roomRepo.getRoomById(UUID.fromString(roomId)) match {
    case Success(Some(result)) => result
    case _ =>
      throw new ApiFailure("IO.MASCARENE.HS.INTERNAL_ERROR", s"Can't start room server for unknown room $roomId")
  }
  private[this] var roomState = RoomState(room)

  implicit val system = context.system
  //implicit private[this] val materializer = Materializer(context)
  private[this] val resolveQueue = Source
    .queue[Event](100, OverflowStrategy.backpressure)
    .map(e => ResolutionContext(e, room))
    .to(
      ActorSink.actorRefWithBackpressure(
        ref = workerRouter,
        onInitMessage = ResolveStreamInit.apply,
        onFailureMessage = ResolveStreamFailure.apply,
        onCompleteMessage = ResolveStreamCompleted,
        ackMessage = ResolveAck,
        messageAdapter = ResolutionRequest.apply
      )
    )
    .run()

  override def onMessage(msg: Command): Behavior[Command] = {
    msg match {
      case GetRoomState(replyTo) =>
        replyTo ! RoomStateResponse(roomState)
        Behaviors.same
      case WrappedQueueOfferResult(response) =>
        context.log.debug(s"queue offer result: $response")
        Behaviors.same
      case WrappedQueueOfferFailure(f) =>
        context.log
          .error("new room event couldn't be pushed to server resolution queue, event will need recovery", f)
        throw f
      case pe: PostEvent =>
        val parentsId: Set[UUID] = if (pe.parentsId.isEmpty && roomState.lastEventId.isDefined) {
          Set(roomState.lastEventId.get)
        } else {
          pe.parentsId
        }
        context.log.debug(
          s"Event posted to room ${room.mxRoomId} : PostEvent(${pe.eventType}, ${pe.stateKey}, ${pe.content}, $parentsId, ${pe.authsEventId})"
        )
        val newEvent =
          insertEvent(roomState.room, pe.eventType, pe.stateKey, pe.senderId, pe.content, parentsId, pe.authsEventId)
        pe.replyTo.foreach(replyTo => replyTo ! RoomEvent(newEvent))

        // go to process event state if the created event is a state event
        newEvent match {
          case Success(event) =>
            val offerResult = resolveQueue.offer(event)
            context.log.debug(s"Event pushed to resolution queue: $event")
            context.pipeToSelf(offerResult) {
              case Success(result) => WrappedQueueOfferResult(result)
              case Failure(f) =>
                WrappedQueueOfferFailure(f)
            }
            roomState = roomState.copy(lastEventId = Some(event.id))
            Behaviors.same
          case Failure(f) =>
            context.log.error("Couldn't insert new event", f)
            Behaviors.same
        }
    }
  }

  /*
  def processingEvent(event: Event, roomState: RoomState): Behavior[Command] = {
    context.log.info(s"Room server processing ${event.eventType} state event ${event.id}")
    context.ask[ResolveRoomState, StateEventProcessed](
      workerRouter,
      ref => ResolveRoomState(Set(event), roomState, ref)
    ) {
      case Success(_) => AdaptedWorkerResponse()
    }
    Behaviors.receiveMessage {
      case GetRoomState(replyTo) =>
        replyTo ! RoomStateResponse(roomState)
        Behaviors.same
      case other =>
        // stash all other messages for later processing
        Behaviors.same
    }
  }

  def finished(event: Event, currentStage: EventProcessStage, room: Room): Behavior[Command] = {
    context.log.info(s"Room server finished processing event ${event.id}")
    eventProcessStageRepo.startNewStage(event.id, processorName, ProcessingStages.FINISHED)
    eventProcessStageRepo.endCurrentStage(event.id, processorName)
    // TODO: delete process_stages for finished stages
    idle(room)
  }

  private def nextProcessingStage(event: Event, currentStage: EventProcessStage, room: Room): Behavior[Command] = {
    if (event.stateKey.isEmpty) {
      //in any case, normal event don't require processing
      finished(event, currentStage, room)
    } else {
      currentStage.stage match {
        case ProcessingStages.CREATION => finished(event, currentStage, room)
      }
    }
  }

   */

  private def insertEvent(
      room: Room,
      eventType: String,
      stateKey: Option[String],
      senderId: UUID,
      content: Option[Json],
      parentsId: Set[UUID] = Set.empty,
      authsEventId: Set[UUID] = Set.empty
  ): Try[Event] = {
    for {
      event <- eventRepo.insertEvent(
        roomId = room.id,
        senderId = senderId,
        eventType = eventType,
        stateKey = stateKey,
        content = content,
        parentsId = parentsId,
        authsEventId = authsEventId,
        originServerTs = LocalDateTime.now()
      )
      mxId <- generateMxEventId(room, event)
    } yield event.copy(mxEventId = Some(mxId))
  }

  private def generateMxEventId(room: Room, event: Event): Try[String] = {
    import io.circe.generic.auto._
    import io.circe.syntax._
    val mxId = room.version match {
      case "1" | "2" =>
        s"$$${Codecs.genId()}:$matrixServerName"
      case "3" | "4" | "5" =>
        time(Logger(context.log), "Event hash computation time") {
          val jsonEvent = event.copy(unsigned = None).asJson.noSpacesSortKeys
          s"$$${Codecs.base64Encode(jsonEvent, unpadded = true)}"
        }
      case _ => throw new ApiFailure("M_UNSUPPORTED_ROOM_VERSION", s"Unsupported room version '${room.version}'")
    }
    eventRepo.updateEventMxId(event.id, Some(mxId)).map(_ => mxId)
  }

}
