/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.server.model

import java.time.LocalDateTime
import java.util.UUID

import scala.util.Try

case class EventProcessStage(
    eventId: UUID,
    processor: String,
    stage: String,
    startedAt: LocalDateTime,
    finishedAt: Option[LocalDateTime] = None
)

class EventProcessStageRepo(dbContext: DbContext) {
  import dbContext._

  private val eventProcessStages = quote(querySchema[EventProcessStage]("event_process_stage"))

  def startNewStage(
      eventId: UUID,
      processor: String,
      stage: String,
      startDate: LocalDateTime = LocalDateTime.now()
  ): Try[EventProcessStage] = {
    val newStage = EventProcessStage(eventId, processor, stage, startDate)

    transaction[Try[EventProcessStage]] {
      for {
        _ <- endCurrentStage(eventId, processor)
        _ <- Try { run(eventProcessStages.insert(lift(newStage))) }
      } yield newStage
    }
  }

  def endCurrentStage(eventId: UUID, processor: String): Try[Long] = Try {
    val now = LocalDateTime.now()
    run {
      quote {
        eventProcessStages
          .filter(_.eventId == lift(eventId))
          .filter(_.processor == lift(processor))
          .filter(_.finishedAt.isEmpty)
          .update(_.finishedAt -> Some(lift(now)))
      }
    }
  }
}
