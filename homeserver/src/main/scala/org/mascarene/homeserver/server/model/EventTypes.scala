/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.server.model

object EventTypes {
  val M_ROOM_CREATE             = "m.room.create"
  val M_ROOM_MEMBER             = "m.room.member"
  val M_ROOM_POWER_LEVELS       = "m.room.power_levels"
  val M_ROOM_JOIN_RULES         = "m.room.join_rules"
  val M_ROOM_HISTORY_VISIBILITY = "m.room.history_visibility"
  val M_ROOM_GUEST_ACCESS       = "m.room.guest_access"
  val M_ROOM_NAME               = "m.room.name"
  val M_ROOM_TOPIC              = "m.room.topic"
  val M_ROOM_ALIASES            = "m.room.aliases"
}
