/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.server.model

import java.io.{ByteArrayOutputStream, Closeable, ObjectOutputStream}
import java.util.UUID

import com.typesafe.config.Config
import redis.clients.jedis.JedisPool
import scalacache._
import scalacache.redis._
import scalacache.modes.try_._
import scalacache.serialization.circe._
import io.circe._
import io.circe.generic.auto._
import org.mascarene.homeserver.server.rooms.StateSet

import scala.jdk.DurationConverters._
import scala.util.Try

class StateSetCache(config: Config, dbContext: DbContext, jedisPool: JedisPool) {
  private[this] val stateSetCache: Cache[StateSet] =
    RedisCache[StateSet](jedisPool)

  private def stateSetKey(eventId: UUID) = s"event(${eventId}).stateSet"

  private val stateSetCacheTtl = Some(config.getDuration("mascarene.server.cache-ttl.state-set").toScala)

  def getStateSet(eventId: UUID): Try[Option[StateSet]] =
    stateSetCache.get(stateSetKey(eventId))
  //.orElse {
  //val event = eventRepo.getEventById(eventId)
  //event.foreach { case Some(e) => eventsCache.put(eventKey(eventId))(e, eventCacheTtl) }
  //event
  //}

  def put(eventId: UUID, stateSet: StateSet) = stateSetCache.put(stateSetKey(eventId))(stateSet, stateSetCacheTtl)
}
