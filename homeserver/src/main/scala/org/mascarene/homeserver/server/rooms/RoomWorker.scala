/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.server.rooms

import akka.actor.typed.{ActorRef, Behavior}
import akka.actor.typed.scaladsl.Behaviors
import com.typesafe.config.Config
import javax.naming.event.EventContext
import org.mascarene.homeserver.server.model.{DbContext, Event, EventRepo, Room}
import redis.clients.jedis.JedisPool
import akka.actor.typed.scaladsl.adapter._
import com.typesafe.scalalogging.Logger
import org.mascarene.utils.LogUtils.time

import scala.util.Try

object RoomWorker {
  trait Ack
  object ResolveAck extends Ack

  sealed trait Response
  case class StateEventProcessed() extends Response
  case object ResolveStreamAck     extends Response

  sealed trait Command
  case class ResolveStreamInit(ackTo: ActorRef[Ack])                                       extends Command
  case object ResolveStreamCompleted                                                       extends Command
  case class ResolveStreamFailure(ex: Throwable)                                           extends Command
  case class ResolutionRequest(ackTo: ActorRef[Ack], resolutionContext: ResolutionContext) extends Command
  case class ResolveRoomState(event: Set[Event], roomState: RoomState, replyTo: ActorRef[StateEventProcessed])
      extends Command

  def apply(config: Config, dbContext: DbContext, jedisPool: JedisPool): Behavior[Command] = Behaviors.setup {
    context =>
      /**
        * Returns a factory method to create a room implementation class given a room version.
        * @param roomVersion room version used to create the implementation class
        * @return a function which can be used to create a room implementation object implementing RoomVersionImpl
        */
      def getRoomImplementationFactory(roomVersion: String): Try[Room => RoomVersionImpl] =
        Try {
          roomVersion match {
            case "1"                   => V1RoomImpl.apply(_)(config, dbContext, jedisPool)
            case "2" | "3" | "4" | "5" => V2RoomImpl.apply(_)(config, dbContext, jedisPool)
          }
        }

      Behaviors.receiveMessage {
        case ResolveStreamFailure(ex) =>
          context.log.error(s"Resolution stream failure", ex)
          Behaviors.same
        case ResolveStreamInit(ackTo) =>
          context.log.debug(s"Stream initialized")
          ackTo ! ResolveAck
          Behaviors.same
        case ResolutionRequest(ackTo, resolutionContext) =>
          getRoomImplementationFactory(resolutionContext.room.version)
            .map { roomImplFactory =>
              val roomImpl = roomImplFactory(resolutionContext.room)
              context.log.debug(s"Resolving event with context $resolutionContext with $roomImpl algorithm")
              time(Logger(context.log), "event resolution time") {
                roomImpl.resolve(resolutionContext.event) match {
                  case Left(rejection) => ???
                  case Right(stateSet) => stateSet
                }
              }
            }
            .recover { f =>
              context.log
                .error(
                  s"Error resolving event ${resolutionContext.event} with room algorithm version ${resolutionContext.room.version}. Cause: {${f.getMessage}"
                )
              context.log.debug("details", f)
            }
          ackTo ! ResolveAck
          Behaviors.same
        case ResolveRoomState(events, roomState, replyTo) =>
          Behaviors.same
        case x =>
          context.log.debug(s"received something else: $x")
          Behaviors.same
      }
  }
}
