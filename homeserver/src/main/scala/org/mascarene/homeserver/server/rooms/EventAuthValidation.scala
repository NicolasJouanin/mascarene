/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.server.rooms

import org.mascarene.homeserver.server.model.{Event, EventRejection, EventTypes}

sealed trait EventAuthValidation {
  def rejection: EventRejection
}
case class MRoomCreateHasNoParent(event: Event, parents: Set[Event]) extends EventAuthValidation {
  override def rejection: EventRejection =
    EventRejection(event.id, Some(s"MRoomCreateHasNoParent: event has one or more previous event: $parents"))
}

case class InvalidIdentifierFormat(message: String, event: Event) extends EventAuthValidation {
  override def rejection: EventRejection =
    EventRejection(event.id, Some(s"InvalidIdentifierFormat: message"))
}
case class RoomDomainMismatchSenderDomain(event: Event) extends EventAuthValidation {
  override def rejection: EventRejection =
    EventRejection(event.id, Some(s"RoomDomainMismatchSenderDomain: Room Id domain doesn't match sender Id domain"))
}
case class DuplicateEntriesInAuthEvents(event: Event) extends EventAuthValidation {
  override def rejection: EventRejection =
    EventRejection(event.id, Some(s"DuplicateEntriesInAuthEvents: Event authchain contains duplicates state entries"))
}
case class NoRoomCreateEventInAuthChain(event: Event) extends EventAuthValidation {
  override def rejection: EventRejection =
    EventRejection(
      event.id,
      Some(s"NoRoomCreateEventInAuthChain: Event authchain doesn't contain a m.room.create event")
    )
}
case class SenderDomainMismatchStateKey(event: Event) extends EventAuthValidation {
  override def rejection: EventRejection =
    EventRejection(event.id, Some(s"SenderDomainMismatchStateKey: sender domain doesn't match state key"))
}
case class InvalidContent(event: Event) extends EventAuthValidation {
  override def rejection: EventRejection =
    EventRejection(event.id, Some("InvalidContent: Event content not provided or couldn't be parsed"))
}
case class ParentEventMustBeUniqueAndCreate(event: Event) extends EventAuthValidation {
  override def rejection: EventRejection =
    EventRejection(
      event.id,
      Some("ParentEventMustBeUniqueAndCreate: parent event is not unique or not a m.room.create event")
    )
}
case class SenderMismatchStatekey(event: Event) extends EventAuthValidation {
  override def rejection: EventRejection =
    EventRejection(event.id, Some("SenderMismatchStatekey: sender doesn't match state key"))
}
case class InternalError(event: Event, ex: Throwable) extends EventAuthValidation {
  override def rejection: EventRejection =
    EventRejection(event.id, Some(s"InternalError: ${ex.getMessage}"))
}
