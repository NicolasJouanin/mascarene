/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.server.model

import java.time.LocalDateTime
import java.util.UUID

import scala.util.Try

case class User(
    userId: UUID,
    mxUserId: String,
    passwordHash: Option[String],
    kind: String,
    createdAt: LocalDateTime,
    updatedAt: Option[LocalDateTime]
)

case class Device(
    deviceId: UUID,
    mxDeviceId: String,
    displayName: String,
    ownerId: UUID,
    lastSeen: Option[LocalDateTime],
    createdAt: LocalDateTime,
    updatedAt: Option[LocalDateTime]
)

case class AuthToken(
    tokenId: UUID,
    userId: UUID,
    deviceId: UUID,
    encodedToken: Option[String],
    lastUsed: Option[LocalDateTime],
    createdAt: LocalDateTime,
    updatedAt: Option[LocalDateTime]
)

class AuthRepo(dbContext: DbContext) {
  import dbContext._

  private val users      = quote(querySchema[User]("users"))
  private val devices    = quote(querySchema[Device]("devices"))
  private val authTokens = quote(querySchema[AuthToken]("auth_tokens"))

  def createUser(mxUserId: String, passwordHash: Option[String], kind: String): Try[User] = Try {
    val newUser = User(UUID.randomUUID(), mxUserId, passwordHash, kind, LocalDateTime.now(), None)
    run {
      users.insert(lift(newUser))
    }
    newUser
  }

  def createDevice(mxDeviceId: String, displayName: String, ownerId: UUID): Try[Device] = Try {
    val newDevice = Device(UUID.randomUUID(), mxDeviceId, displayName, ownerId, None, LocalDateTime.now(), None)
    run {
      devices.insert(lift(newDevice))
    }
    newDevice
  }

  def addToken(userId: UUID, deviceId: UUID, encodedToken: Option[String]): Try[AuthToken] = Try {
    val newToken = AuthToken(UUID.randomUUID(), userId, deviceId, encodedToken, None, LocalDateTime.now(), None)
    run {
      authTokens.insert(lift(newToken))
    }
    newToken
  }

  def addOrUpdateToken(userId: UUID, deviceId: UUID, encodedToken: Option[String]): Try[AuthToken] = {
    getToken(userId, deviceId) flatMap {
      case None => addToken(userId, deviceId, encodedToken)
      case Some(authToken) =>
        updateEncodedToken(authToken.tokenId, encodedToken).map(_ =>
          authToken.copy(encodedToken = encodedToken, updatedAt = Some(LocalDateTime.now()))
        )
    }
  }

  def updateEncodedToken(tokenId: UUID, encodedToken: Option[String]): Try[Long] = Try {
    val now: Option[LocalDateTime] = Some(LocalDateTime.now())
    run {
      authTokens
        .filter(_.tokenId == lift(tokenId))
        .update(_.updatedAt -> lift(now), _.encodedToken -> lift(encodedToken))
    }
  }

  def createUserWithDevice(
      mxUserId: String,
      passwordHash: Option[String],
      kind: String,
      mxDeviceId: String,
      displayName: String
  ): Try[(User, Device)] =
    transaction[Try[(User, Device)]] {
      for {
        user   <- createUser(mxUserId, passwordHash, kind)
        device <- createDevice(mxDeviceId, displayName, user.userId)
      } yield (user, device)
    }

  def getToken(userId: UUID, deviceId: UUID): Try[Option[AuthToken]] = Try {
    run(authTokens.filter(_.userId == lift(userId)).filter(_.deviceId == lift(deviceId))).headOption
  }
  def getUserByMxId(mxid: String): Try[Option[User]] = Try { run(users.filter(_.mxUserId == lift(mxid))).headOption }

  def getUserById(userId: UUID): Try[Option[User]] = Try { run(users.filter(_.userId == lift(userId))).headOption }

  def getDeviceByMxId(mxDeviceId: String): Try[Option[Device]] = Try {
    run(devices.filter(_.mxDeviceId == lift(mxDeviceId))).headOption
  }

  def getUserDevices(userId: UUID): Try[List[Device]] = Try {
    run {
      devices.filter(_.ownerId == lift(userId))
    }
  }

  def getCredentials(tokenId: UUID): Try[Option[(User, Device, AuthToken)]] = Try {
    run {
      for {
        authToken <- authTokens
        device    <- devices.join(_.deviceId == authToken.deviceId)
        user      <- users.join(_.userId == authToken.userId)
      } yield (user, device, authToken)
    }.headOption
  }

  def deleteDevice(deviceId: UUID): Try[Unit] = Try {
    run(devices.filter(_.deviceId == lift(deviceId)).delete)
  }

}
