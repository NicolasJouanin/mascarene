/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.server.model

import java.sql.PreparedStatement
import java.time.LocalDateTime
import java.util.UUID

import org.mascarene.matrix.client.r0.model.filter.FilterDefinition
import io.circe.generic.auto._
import io.circe.syntax._
import io.circe.parser.decode

import scala.util.Try

case class Filter(
    id: UUID,
    userId: UUID,
    filterDefinition: Option[FilterDefinition] = None,
    createdAt: LocalDateTime,
    updatedAt: Option[LocalDateTime]
)

class FilteringRepo(dbContext: DbContext) {
  import dbContext._

  private val filters = quote(dbContext.querySchema[Filter]("filters"))

  private implicit val fdDecoder: Decoder[FilterDefinition] =
    decoder((index, row) =>
      decode[FilterDefinition](row.getObject(index).toString).fold(error => throw error, fd => fd)
    )

  private implicit val fdEncoder: Encoder[FilterDefinition] = encoder[FilterDefinition](
    java.sql.Types.OTHER,
    (index: Int, value: FilterDefinition, row: PreparedStatement) =>
      row.setObject(index, value.asJson.noSpaces, java.sql.Types.OTHER)
  )

  def createFilter(userId: UUID, filterDefinition: FilterDefinition): Try[Filter] = Try {
    val newRow = Filter(UUID.randomUUID(), userId, Some(filterDefinition), LocalDateTime.now(), None)
    run {
      filters.insert(lift(newRow))
    }
    newRow
  }

  def getFilter(filterId: UUID): Try[Option[Filter]] = Try {
    import dbContext._
    run { filters.filter(_.id == lift(filterId)) }.headOption
  }
}
