/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.matrix.client.filtering

import java.util.UUID

import com.typesafe.config.Config
import org.mascarene.homeserver.server.model.DbContext
import org.mascarene.matrix.client.r0.model.filter.FilterDefinition
import org.mascarene.homeserver.server.model.{Filter, FilteringRepo}

import scala.util.Try

class FilteringApi(config: Config, dbContext: DbContext) {
  private val filteringRepo = new FilteringRepo(dbContext)

  def createFilter(userId: UUID, filterDefinition: FilterDefinition): Try[Filter] =
    filteringRepo.createFilter(userId, filterDefinition)

  def getFilter(filterId: UUID): Try[Option[Filter]] = filteringRepo.getFilter(filterId)
}
