/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.matrix.client

import akka.actor.typed.ActorSystem
import akka.http.scaladsl.server.Directives.pathPrefix
import akka.http.scaladsl.server.{PathMatcher, Route}
import com.typesafe.config.Config
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, StatusCodes}
import io.circe.Printer
import io.circe.generic.auto._
import org.mascarene.homeserver.server.model.DbContext
import org.mascarene.homeserver.ImplicitAskTimeOut
import org.mascarene.homeserver.matrix.{ApiErrorRejection, ApiErrors}
import org.mascarene.homeserver.matrix.client.auth.{AuthApi, AuthDirectives}

class AccountDataApiRoutes(
    val config: Config,
    dbContext: DbContext,
    implicit val system: ActorSystem[_]
) extends FailFastCirceSupport
    with AuthDirectives
    with ImplicitAskTimeOut {
  implicit val printer: Printer = Printer.noSpaces.copy(dropNullValues = true)

  private val accountDataApi = new AccountDataApi(dbContext)
  protected val authApi      = new AuthApi(config, dbContext, system)

  lazy val routes: Route = pathPrefix("_matrix" / "client" / "r0" / "user") {
    path(Segment / "account_data" / Segment) { (mxUserId, dataType) =>
      requireAuth { credentials =>
        get {
          if (credentials.user.mxUserId == mxUserId) {
            accountDataApi
              .getAccountData(credentials.user.userId, dataType)
              .map {
                case Some(data) => complete(HttpEntity(ContentTypes.`application/json`, data.eventContent))
                case None       => complete(StatusCodes.NotFound)
              }
              .recover(failure => reject(ApiErrorRejection(ApiErrors.InternalError(Some(failure.getMessage)))))
              .get
          } else {
            reject(
              ApiErrorRejection(
                ApiErrors.ForbiddenError(Some(s"API path userId '$mxUserId' doesn't match authorized user id"))
              )
            )
          }
        } ~
          put {
            entity(as[String]) { value =>
              if (credentials.user.mxUserId == mxUserId) {
                accountDataApi
                  .storeAccountData(credentials.user.userId, dataType, value)
                  .map(_ => complete(()))
                  .recover(failure => reject(ApiErrorRejection(ApiErrors.InternalError(Some(failure.getMessage)))))
                  .get
              } else {
                reject(
                  ApiErrorRejection(
                    ApiErrors.ForbiddenError(Some(s"API path userId '$mxUserId' doesn't match authorized user id"))
                  )
                )
              }
            }
          }
      }
    }
  }
}
