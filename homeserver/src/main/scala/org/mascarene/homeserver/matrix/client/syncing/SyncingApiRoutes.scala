/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.matrix.client.syncing

import akka.actor.typed.ActorSystem
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.typesafe.config.Config
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.Printer
import org.mascarene.homeserver.server.model.DbContext
import org.mascarene.homeserver.ImplicitAskTimeOut
import org.mascarene.homeserver.matrix.client.auth.{AuthApi, AuthDirectives}

class SyncingApiRoutes(
    val config: Config,
    dbContext: DbContext,
    implicit val system: ActorSystem[_]
) extends FailFastCirceSupport
    with AuthDirectives
    with ImplicitAskTimeOut {
  implicit val printer: Printer = Printer.noSpaces.copy(dropNullValues = true)

  protected val authApi = new AuthApi(config, dbContext, system)

  lazy val routes: Route = pathPrefix("_matrix" / "client" / "r0") {
    path("sync") {
      get {
        requireAuth { credentials =>
          parameters("filter".?, "since".?, "full_state".as[Boolean].?, "set_presence".?, "timeout".as[Int].?) {
            (filter, since, fullState, setPresence, timeout) => complete(())
          }
        }
      }
    }
  }

}
