/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.matrix.client.filtering

import java.util.UUID

import akka.actor.typed.ActorSystem
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.typesafe.config.Config
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.Printer
import io.circe.generic.auto._
import org.mascarene.homeserver.matrix.ApiErrorRejection
import org.mascarene.homeserver.matrix.client.auth.AuthDirectives
import akka.http.scaladsl.model.StatusCodes
import org.mascarene.homeserver.server.model.DbContext
import org.mascarene.matrix.client.r0.model.filter.{FilterDefinition, PostFilterResponse}
import org.mascarene.homeserver.ImplicitAskTimeOut
import org.mascarene.homeserver.matrix.{ApiErrorRejection, ApiErrors}
import org.mascarene.homeserver.matrix.client.auth.{AuthApi, AuthDirectives}
import org.mascarene.utils.Codecs

class FilteringApiRoutes(
    val config: Config,
    dbContext: DbContext,
    implicit val system: ActorSystem[_]
) extends FailFastCirceSupport
    with AuthDirectives
    with ImplicitAskTimeOut {
  implicit val printer: Printer = Printer.noSpaces.copy(dropNullValues = true)

  private val filteringApi = new FilteringApi(config, dbContext)
  protected val authApi    = new AuthApi(config, dbContext, system)

  lazy val routes: Route = pathPrefix("_matrix" / "client" / "r0" / "user") {
    path(Segment / "filter") { mxUserId =>
      requireAuth { credentials =>
        post {
          if (credentials.user.mxUserId == mxUserId) {
            entity(as[FilterDefinition]) { filterDefinition =>
              filteringApi
                .createFilter(credentials.user.userId, filterDefinition)
                .map(filter => complete(PostFilterResponse(Codecs.base64Encode(filter.id.toString))))
                .recover(failure => reject(ApiErrorRejection(ApiErrors.InternalError(Some(failure.getMessage)))))
                .get
            }
          } else {
            reject(
              ApiErrorRejection(
                ApiErrors.ForbiddenError(Some(s"API path userId '$mxUserId' doesn't match authorized user id"))
              )
            )
          }
        }
      }
    }
    path(Segment / "filter" / Segment) { (mxUserId, encodedFilterId) =>
      requireAuth { credentials =>
        get {
          if (credentials.user.mxUserId == mxUserId) {
            filteringApi
              .getFilter(UUID.fromString(Codecs.base64Decode(encodedFilterId)))
              .map {
                case Some(filter) => complete(filter.filterDefinition)
                case None         => complete(StatusCodes.NotFound)

              }
              .recover(failure => reject(ApiErrorRejection(ApiErrors.InternalError(Some(failure.getMessage)))))
              .get
          } else {
            reject(
              ApiErrorRejection(
                ApiErrors.ForbiddenError(Some(s"API path userId '$mxUserId' doesn't match authorized user id"))
              )
            )
          }
        }
      }
    }
  }
}
