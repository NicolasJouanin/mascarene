/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver

import java.io.Closeable

import akka.actor.typed.ActorSystem
import com.typesafe.config.{Config, ConfigFactory, ConfigValueFactory}
import com.typesafe.scalalogging.LazyLogging
import inet.ipaddr.HostName
import io.getquill.{PostgresJdbcContext, SnakeCase}
import MascareneServer.Stop
import de.mkammerer.argon2.{Argon2Factory, Argon2Helper}
import org.mascarene.homeserver.server.model.DbContext
import org.mascarene.homeserver.version.BuildInfo
import org.flywaydb.core.Flyway
import javax.sql.DataSource
import org.mascarene.utils.LogUtils.time
import redis.clients.jedis.{JedisPool, JedisPoolConfig}

import scala.util.{Failure, Success, Try}

object MascareneMain extends LazyLogging {
  def main(args: Array[String]): Unit = {
    logger.info(s"Mascarene (${BuildInfo.version}) is starting up ...")

    val status = for {
      config                 <- time(logger, "Configuration load") { Try { ConfigFactory.load() } }
      configWithArgon2       <- time(logger, "Argon2 tuning") { Try { initArgon2Parameters(config) } }
      (dbContext, jedisPool) <- initDbContext(configWithArgon2)
    } yield (configWithArgon2, dbContext, jedisPool)

    status match {
      case Success((config, dbContext, jedisPool)) =>
        logger.info("Mascarene init completed")
        val trySystem = startMascareneServer(config, dbContext, jedisPool)
        trySystem match {
          case Success(_) => logger.info("Mascarene started successfully.")
          case Failure(f) =>
            logger.warn(s"Mascarene stopped with failure: ${f.getMessage}")
            logger.debug("failure details: ", f)
        }

        val mainThread = Thread.currentThread()
        Runtime.getRuntime.addShutdownHook(new Thread() {
          override def run(): Unit = {
            trySystem.map(system => system ! Stop)
            mainThread.join()
          }
        })
      case Failure(f) =>
        logger.error(s"Mascarene startup failed due to error: ${f.getMessage}")
        logger.debug("error details", f)
        System.exit(-1)
    }
  }

  private def startMascareneServer(
      config: Config,
      dbContext: DbContext,
      jedisPool: JedisPool
  ): Try[ActorSystem[MascareneServer.Message]] = {
    val bindAddress = config.getString("mascarene.server.bind-address")
    val host        = new HostName(bindAddress)
    if (host.isValid) {
      Success(
        ActorSystem[MascareneServer.Message](
          MascareneServer(host.getHost, host.getPort, config, dbContext, jedisPool),
          s"MascareneHS",
          config
        )
      )
    } else {
      Failure(new IllegalArgumentException(s"Malformed mascarene.server.bind-address parameter: '$bindAddress'"))
    }
  }

  private def initDbContext(config: Config): Try[(DbContext, JedisPool)] = {
    for {
      dataSource <- createDataSource(config)
      _          <- migrateDB(dataSource)
      jedisPool  <- createJedisPool(config)
    } yield (new PostgresJdbcContext(SnakeCase, dataSource), jedisPool)
  }

  private def initArgon2Parameters(config: Config): Config = {
    val argonMemCost     = config.getInt("mascarene.server.auth.hash.mem-cost")
    val argonParallelism = config.getInt("mascarene.server.auth.hash.parallelism")
    val argonTimeCost    = config.getDuration("mascarene.server.auth.hash.time-cost").toMillis
    logger.info(s"Tuning Argon2 hash parameters ...")
    val argon2 = Argon2Factory.create(
      config.getInt("mascarene.server.auth.hash.salt-length"),
      config.getInt("mascarene.server.auth.hash.hash-length")
    )
    val argonIterations = Argon2Helper.findIterations(argon2, argonTimeCost, argonMemCost, argonParallelism)
    logger.debug(
      s"Argon2 hash parameters: memCost=$argonMemCost, parallelism=$argonParallelism, timeCost=$argonTimeCost, iterations=$argonIterations"
    )
    config.withValue("mascarene.server.auth.hash.iterations", ConfigValueFactory.fromAnyRef(argonIterations))
  }

  private def createDataSource(config: Config): Try[DataSource with Closeable] = {
    import com.zaxxer.hikari.HikariDataSource
    Try {
      val dbConfig = config.getConfig("mascarene.db")
      val ds       = new HikariDataSource()
      ds.setDataSourceClassName(dbConfig.getString("datasource-class-name"))
      ds.addDataSourceProperty("url", dbConfig.getString("url"))
      ds.addDataSourceProperty("user", dbConfig.getString("username"))
      ds.addDataSourceProperty("password", dbConfig.getString("password"))
      ds
    }
  }

  private def createJedisPool(config: Config): Try[JedisPool] = Try {
    val redisConfig = config.getConfig("mascarene.redis")
    new JedisPool(new JedisPoolConfig(), redisConfig.getString("host"), redisConfig.getInt("port"))
  }

  private def migrateDB(dataSource: DataSource): Try[Unit] = {
    time(logger, "database migration") {
      val flyway = Flyway.configure.dataSource(dataSource).load
      Try { flyway.migrate() }
    }
  }
}
