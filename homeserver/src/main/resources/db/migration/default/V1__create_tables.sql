CREATE TABLE users(
    user_id UUID PRIMARY KEY ,
    mx_user_id VARCHAR NOT NULL,
    password_hash VARCHAR,
    kind VARCHAR CHECK(kind in ('user', 'guest')),
    created_at TIMESTAMP,
    updated_at TIMESTAMP
);

CREATE TABLE devices (
  device_id UUID PRIMARY KEY,
  mx_device_id VARCHAR NOT NULL,
  display_name VARCHAR,
  owner_id UUID REFERENCES users(user_id) ON DELETE CASCADE,
  last_seen TIMESTAMP,
  created_at TIMESTAMP NOT NULL,
  updated_at TIMESTAMP
);

CREATE TABLE auth_tokens(
    token_id UUID PRIMARY KEY,
    user_id UUID REFERENCES users(user_id) ON DELETE CASCADE,
    device_id UUID REFERENCES devices(device_id) ON DELETE CASCADE,
    encoded_token VARCHAR,
    last_used TIMESTAMP,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP
);

CREATE TABLE account_data(
    id UUID PRIMARY KEY ,
    user_id UUID REFERENCES users(user_id) ON DELETE CASCADE,
    event_type VARCHAR NOT NULL,
    event_content TEXT,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP
);

CREATE TABLE filters(
    id UUID PRIMARY KEY ,
    user_id UUID REFERENCES users(user_id) ON DELETE CASCADE,
    filter_definition JSONB,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP
);

CREATE TABLE rooms(
    id UUID PRIMARY KEY,
    mx_room_id VARCHAR NOT NULL,
    visibility VARCHAR NOT NULL,
    version VARCHAR NOT NULL,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP
);

CREATE TABLE events(
    id UUID PRIMARY KEY,
    mx_event_id VARCHAR,
    state_key VARCHAR,
    sender_id UUID REFERENCES users(user_id),
    room_id UUID REFERENCES rooms(id),
    event_type VARCHAR NOT NULL,
    origin_server_ts TIMESTAMP,
    unsigned JSONB,
    received_ts TIMESTAMP,
    created_at TIMESTAMP NOT NULL
);

CREATE TABLE event_auth_edges(
    event_id UUID REFERENCES events(id),
    auth_event_mx_id VARCHAR,
    auth_event_id UUID REFERENCES events(id)
);

CREATE TABLE event_parent_edges(
    event_id UUID REFERENCES events(id),
    parent_event_mx_id VARCHAR,
    parent_event_id UUID REFERENCES events(id)
);

CREATE TABLE event_rejections(
    event_id UUID REFERENCES events(id),
    cause VARCHAR
);

CREATE TABLE event_contents(
    event_id UUID REFERENCES events(id) ON DELETE CASCADE,
    content JSONB
);

CREATE TABLE event_process_stage(
    event_id UUID REFERENCES events(id) ON DELETE CASCADE,
    processor VARCHAR NOT NULL,
    stage VARCHAR NOT NULL,
    started_at TIMESTAMP,
    finished_at TIMESTAMP
);