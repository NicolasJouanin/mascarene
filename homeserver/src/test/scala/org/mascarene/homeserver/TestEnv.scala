/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver

import com.typesafe.config.{Config, ConfigFactory, ConfigValueFactory}
import de.mkammerer.argon2.{Argon2Factory, Argon2Helper}
import io.getquill.{PostgresJdbcContext, SnakeCase}
import org.mascarene.homeserver.server.model.DbContext
import org.flywaydb.core.Flyway
import org.scalatest.{BeforeAndAfter, Suite}
import redis.clients.jedis.{JedisPool, JedisPoolConfig}

import scala.util.Try

trait TestEnv extends Suite with BeforeAndAfter {
  import com.zaxxer.hikari.HikariDataSource

  protected val config: Config = initArgon2Parameters(ConfigFactory.load("application-test.conf"))
  private val dbConfig         = config.getConfig("mascarene.db")
  val datasource               = new HikariDataSource()
  datasource.setDataSourceClassName(dbConfig.getString("datasource-class-name"))
  datasource.addDataSourceProperty("url", dbConfig.getString("url"))
  datasource.addDataSourceProperty("user", dbConfig.getString("username"))
  datasource.addDataSourceProperty("password", dbConfig.getString("password"))
  val flyway = Flyway.configure.dataSource(datasource).load

  val redisConfig = config.getConfig("mascarene.redis")
  val jedisPool   = new JedisPool(new JedisPoolConfig(), redisConfig.getString("host"), redisConfig.getInt("port"))

  protected val dbContext: DbContext = new PostgresJdbcContext(SnakeCase, datasource)

  private def initArgon2Parameters(config: Config): Config = {
    val argonMemCost     = config.getInt("mascarene.server.auth.hash.mem-cost")
    val argonParallelism = config.getInt("mascarene.server.auth.hash.parallelism")
    val argonTimeCost    = config.getDuration("mascarene.server.auth.hash.time-cost").toMillis
    val argon2 = Argon2Factory.create(
      config.getInt("mascarene.server.auth.hash.salt-length"),
      config.getInt("mascarene.server.auth.hash.hash-length")
    )
    val argonIterations = Argon2Helper.findIterations(argon2, argonTimeCost, argonMemCost, argonParallelism)
    config.withValue("mascarene.server.auth.hash.iterations", ConfigValueFactory.fromAnyRef(argonIterations))
  }

  before {
    flyway.clean()
    flyway.migrate()
  }

  after {
    //flyway.clean()
  }
}
