/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.server.room

import java.time.LocalDateTime
import java.util.UUID

import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import org.mascarene.homeserver.server.model.{AuthRepo, EventRepo, EventTypes, Room, RoomRepo, User}
import org.mascarene.homeserver.server.rooms.{PowerLevelsUtils, V2RoomImpl}
import org.mascarene.matrix.client.r0.model.rooms.CreationContent
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import io.circe.generic.auto._
import io.circe.syntax._
import org.mascarene.homeserver.TestEnv
import org.mascarene.matrix.client.r0.model.events.MemberEventContent
import org.mascarene.utils.Codecs

class V2RoomImplSpec extends AnyWordSpecLike with Matchers with FailFastCirceSupport with TestEnv {

  private[this] val eventRepo = new EventRepo(dbContext)
  private[this] val authRepo  = new AuthRepo(dbContext)
  private[this] val roomRepo  = new RoomRepo(dbContext)

  private def insertCreateEvent(room: Room, sender: User) =
    eventRepo
      .insertEvent(
        room.id,
        sender.userId,
        EventTypes.M_ROOM_CREATE,
        Some(""),
        Some(CreationContent(creator = sender.mxUserId).asJson),
        mxEventId = Some(s"$$create${Codecs.genId()}"),
        originServerTs = LocalDateTime.now()
      )
      .get

  private def insertMemberEvent(room: Room, sender: User, parentsId: Set[UUID], authEventsId: Set[UUID]) =
    eventRepo
      .insertEvent(
        room.id,
        sender.userId,
        EventTypes.M_ROOM_MEMBER,
        Some(sender.mxUserId),
        Some(MemberEventContent(membership = "join", is_direct = false).asJson),
        mxEventId = Some(s"$$member${Codecs.genId()}"),
        originServerTs = LocalDateTime.now(),
        parentsId = parentsId,
        authsEventId = authEventsId
      )
      .get

  private def insertPLEvent(room: Room, sender: User, parentsId: Set[UUID], authEventsId: Set[UUID]) =
    eventRepo
      .insertEvent(
        room.id,
        sender.userId,
        EventTypes.M_ROOM_POWER_LEVELS,
        Some(""),
        Some(PowerLevelsUtils.defaultPowerLevelContent.copy(users = Map(sender.mxUserId -> 100)).asJson),
        mxEventId = Some(s"$$powerlevel${Codecs.genId()}"),
        originServerTs = LocalDateTime.now(),
        parentsId = parentsId,
        authsEventId = authEventsId
      )
      .get

  private def insertJoinRulesEvent(room: Room, sender: User, parentsId: Set[UUID], authEventsId: Set[UUID]) =
    eventRepo
      .insertEvent(
        room.id,
        sender.userId,
        EventTypes.M_ROOM_JOIN_RULES,
        Some(""),
        Some(Map("join_rule" -> "private").asJson),
        mxEventId = Some(s"$$joinrules${Codecs.genId()}"),
        originServerTs = LocalDateTime.now(),
        parentsId = parentsId,
        authsEventId = authEventsId
      )
      .get

  private def insertInviteEvent(
      room: Room,
      sender: User,
      invitee: User,
      parentsId: Set[UUID],
      authEventsId: Set[UUID]
  ) =
    eventRepo
      .insertEvent(
        room.id,
        sender.userId,
        EventTypes.M_ROOM_MEMBER,
        Some(invitee.mxUserId),
        Some(MemberEventContent(membership = "invite", is_direct = false).asJson),
        mxEventId = Some(s"$$member${Codecs.genId()}"),
        originServerTs = LocalDateTime.now(),
        parentsId = parentsId,
        authsEventId = authEventsId
      )
      .get

  "v2RoomImpl resolve algorithm" should {

    "resolve direct graph" in {
      val alice    = authRepo.createUser("@alice:example.com", None, "user").get
      val bob      = authRepo.createUser("@bob:example.com", None, "user").get
      val carol    = authRepo.createUser("@carol:example.com", None, "user").get
      val testRoom = roomRepo.createRoom("!room:example.com", "public", "5").get

      val createEvent     = insertCreateEvent(testRoom, alice)
      val aliceJoinEvent  = insertMemberEvent(testRoom, alice, Set(createEvent.id), Set(createEvent.id))
      val powerLevelEvent = insertPLEvent(testRoom, alice, Set(aliceJoinEvent.id), Set(aliceJoinEvent.id))
      val joinRulesEvent =
        insertJoinRulesEvent(testRoom, alice, Set(powerLevelEvent.id), Set(powerLevelEvent.id, aliceJoinEvent.id))
      val inviteBob =
        insertInviteEvent(testRoom, alice, bob, Set(joinRulesEvent.id), Set(aliceJoinEvent.id, powerLevelEvent.id))
      val inviteCarol =
        insertInviteEvent(testRoom, alice, carol, Set(inviteBob.id), Set(aliceJoinEvent.id, powerLevelEvent.id))
      val bobJoinEvent = insertMemberEvent(testRoom, bob, Set(inviteCarol.id), Set(joinRulesEvent.id, inviteBob.id))

      val algoImpl = new V2RoomImpl(testRoom)(config, dbContext, jedisPool)
      algoImpl.resolve(createEvent) match {
        case Right(stateSet) =>
          assert(stateSet.size == 1)
          stateSet.getEvent(EventTypes.M_ROOM_CREATE, "") shouldBe Some(createEvent)
        case Left(x) => fail(x.toString)
      }

      algoImpl
        .resolve(aliceJoinEvent) match {
        case Right(stateSet) =>
          assert(stateSet.size == 2)
          stateSet.getEvent(EventTypes.M_ROOM_CREATE, "") shouldBe Some(createEvent)
          stateSet.getEvent(EventTypes.M_ROOM_MEMBER, alice.mxUserId) shouldBe Some(aliceJoinEvent)
        case Left(x) => fail(x.toString)
      }
      algoImpl
        .resolve(powerLevelEvent) match {
        case Right(stateSet) =>
          assert(stateSet.size == 3)
          stateSet.getEvent(EventTypes.M_ROOM_CREATE, "") shouldBe Some(createEvent)
          stateSet.getEvent(EventTypes.M_ROOM_MEMBER, alice.mxUserId) shouldBe Some(aliceJoinEvent)
          stateSet.getEvent(EventTypes.M_ROOM_POWER_LEVELS, "") shouldBe Some(powerLevelEvent)
        case Left(x) => fail(x.toString)
      }
      algoImpl
        .resolve(joinRulesEvent) match {
        case Right(stateSet) =>
          assert(stateSet.size == 4)
          stateSet.getEvent(EventTypes.M_ROOM_CREATE, "") shouldBe Some(createEvent)
          stateSet.getEvent(EventTypes.M_ROOM_MEMBER, alice.mxUserId) shouldBe Some(aliceJoinEvent)
          stateSet.getEvent(EventTypes.M_ROOM_POWER_LEVELS, "") shouldBe Some(powerLevelEvent)
          stateSet.getEvent(EventTypes.M_ROOM_JOIN_RULES, "") shouldBe Some(joinRulesEvent)
        case Left(x) => fail(x.toString)
      }
      algoImpl
        .resolve(inviteBob) match {
        case Right(stateSet) =>
          assert(stateSet.size == 5)
          stateSet.getEvent(EventTypes.M_ROOM_CREATE, "") shouldBe Some(createEvent)
          stateSet.getEvent(EventTypes.M_ROOM_MEMBER, alice.mxUserId) shouldBe Some(aliceJoinEvent)
          stateSet.getEvent(EventTypes.M_ROOM_POWER_LEVELS, "") shouldBe Some(powerLevelEvent)
          stateSet.getEvent(EventTypes.M_ROOM_JOIN_RULES, "") shouldBe Some(joinRulesEvent)
          stateSet.getEvent(EventTypes.M_ROOM_MEMBER, bob.mxUserId) shouldBe Some(inviteBob)
        case Left(x) => fail(x.toString)
      }
      algoImpl
        .resolve(inviteCarol) match {
        case Right(stateSet) =>
          assert(stateSet.size == 6)
          stateSet.getEvent(EventTypes.M_ROOM_CREATE, "") shouldBe Some(createEvent)
          stateSet.getEvent(EventTypes.M_ROOM_MEMBER, alice.mxUserId) shouldBe Some(aliceJoinEvent)
          stateSet.getEvent(EventTypes.M_ROOM_POWER_LEVELS, "") shouldBe Some(powerLevelEvent)
          stateSet.getEvent(EventTypes.M_ROOM_JOIN_RULES, "") shouldBe Some(joinRulesEvent)
          stateSet.getEvent(EventTypes.M_ROOM_MEMBER, carol.mxUserId) shouldBe Some(inviteCarol)
          stateSet.getEvent(EventTypes.M_ROOM_MEMBER, bob.mxUserId) shouldBe Some(inviteBob)
        case Left(x) => fail(x.toString)
      }
      algoImpl
        .resolve(bobJoinEvent) match {
        case Right(stateSet) =>
          assert(stateSet.size == 6)
          stateSet.getEvent(EventTypes.M_ROOM_CREATE, "") shouldBe Some(createEvent)
          stateSet.getEvent(EventTypes.M_ROOM_MEMBER, alice.mxUserId) shouldBe Some(aliceJoinEvent)
          stateSet.getEvent(EventTypes.M_ROOM_POWER_LEVELS, "") shouldBe Some(powerLevelEvent)
          stateSet.getEvent(EventTypes.M_ROOM_JOIN_RULES, "") shouldBe Some(joinRulesEvent)
          stateSet.getEvent(EventTypes.M_ROOM_MEMBER, carol.mxUserId) shouldBe Some(inviteCarol)
          stateSet.getEvent(EventTypes.M_ROOM_MEMBER, bob.mxUserId) shouldBe Some(bobJoinEvent)
        case Left(x) => fail(x.toString)
      }
    }
  }
}
