/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.matrix.client

import akka.actor.testkit.typed.scaladsl.ActorTestKit
import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.actor.typed.scaladsl.adapter._
import akka.http.scaladsl.model.StatusCodes
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.generic.auto._
import org.mascarene.homeserver.TestEnv
import org.mascarene.homeserver.matrix.client.auth.AuthApiRoutes
import org.mascarene.matrix.client.r0.model.auth.{
  AuthFlow,
  LoginRequest,
  LoginResponse,
  RegisterRequest,
  RegisterResponse
}
import org.scalatest.{BeforeAndAfter, _}
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike

class AuthApiSpec
    extends AnyWordSpecLike
    with Matchers
    with FailFastCirceSupport
    with ScalatestRouteTest
    with TestEnv
    with BeforeAndAfterAll {
  lazy val testKit                                         = ActorTestKit("MascareneTEST")
  implicit def typedSystem                                 = testKit.system
  override def createActorSystem(): akka.actor.ActorSystem = testKit.system.toClassic

  private val authApiRoutes = new AuthApiRoutes(config, dbContext, testKit.system)

  "Auth API" should {
    "return auth flow data from first registration with auth data " in {
      Post("/_matrix/client/r0/register", RegisterRequest()) ~> authApiRoutes.routes ~> check {
        val flow = responseAs[AuthFlow]
      }
    }

    "return completed auth stages with dummy flow and complete register" in {
      Post("/_matrix/client/r0/register", RegisterRequest()) ~> authApiRoutes.routes ~> check {
        status shouldEqual StatusCodes.Unauthorized
        val flow        = responseAs[AuthFlow]
        val authRequest = RegisterRequest(auth = Some(Map("type" -> "m.login.dummy", "session" -> flow.session.get)))
        Post("/_matrix/client/r0/register", authRequest) ~> authApiRoutes.routes ~> check {
          status shouldEqual StatusCodes.OK
          val registerResponse = responseAs[RegisterResponse]
          registerResponse.user_id should not be empty
          registerResponse.device_id should not be empty
          registerResponse.access_token should not be empty
        }
      }
    }

    "complete login" in {
      Post("/_matrix/client/r0/register", RegisterRequest()) ~> authApiRoutes.routes ~> check {
        status shouldEqual StatusCodes.Unauthorized
        val flow = responseAs[AuthFlow]
        val authRequest = RegisterRequest(
          auth = Some(Map("type" -> "m.login.dummy", "session" -> flow.session.get)),
          username = Some("someuser"),
          password = Some("password")
        )
        Post("/_matrix/client/r0/register", authRequest) ~> authApiRoutes.routes ~> check {
          status shouldEqual StatusCodes.OK
          val registerResponse = responseAs[RegisterResponse]
          registerResponse.user_id should not be empty
          registerResponse.device_id should not be empty
          registerResponse.access_token should not be empty

          val loginRequest =
            LoginRequest("m.login.password", Map("type" -> "user", "user" -> "someuser"), Some("password"))
          Post("/_matrix/client/r0/login", loginRequest) ~> authApiRoutes.routes ~> check {
            status shouldEqual StatusCodes.OK
            val registerResponse = responseAs[LoginResponse]
          }
        }
      }
    }

  }
  override def afterAll(): Unit = testKit.shutdownTestKit()
}
