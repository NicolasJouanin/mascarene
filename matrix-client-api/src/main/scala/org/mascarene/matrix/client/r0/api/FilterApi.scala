/*
Copyright 2018 Nicolas Jouanin

This file is part of Plasma.

Plasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License 3.

Plasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>
 */
package org.mascarene.matrix.client.r0.api

import akka.actor.ActorSystem
import akka.http.scaladsl.model.Uri
import akka.stream.Materializer
import io.circe.generic.auto._
import org.mascarene.matrix.client.r0.model.filter._
import org.mascarene.sdk.matrix.core.Transport
import org.mascarene.sdk.matrix.core.model.AuthToken

import scala.concurrent.{ExecutionContextExecutor, Future}

trait FilterApi { self: Transport =>
  implicit def system: ActorSystem
  implicit def materializer: Materializer
  implicit def executionContext: ExecutionContextExecutor

  protected def apiRoot: Uri

  def uploadFilter(userId: String, request: FilterDefinition)(implicit token: AuthToken): Future[PostFilterResponse] =
    doPost[FilterDefinition, PostFilterResponse](apiRoot.withPath(apiRoot.path + s"/r0/user/$userId/filter"), request)

  def getFilter(userId: String, filterId: String)(implicit token: AuthToken): Future[GetFilterResponse] =
    doGet[GetFilterResponse](apiRoot.withPath(apiRoot.path + s"/r0/user/$userId/filter/$filterId"))
}
