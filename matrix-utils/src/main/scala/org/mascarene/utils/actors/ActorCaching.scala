/*
Copyright 2018 Nicolas Jouanin

This file is part of Plasma.

Plasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License 3.

Plasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>
 */
package org.mascarene.utils.actors

import akka.actor.{Actor, Timers}
import com.typesafe.scalalogging.LazyLogging

import scala.collection.mutable.Map
import scala.concurrent.Future
import scala.concurrent.duration.{Duration, FiniteDuration}

case class SetCacheDuration(duration: FiniteDuration)

trait ActorCaching[K, V] extends LazyLogging with Timers { this: Actor =>
  import akka.pattern.pipe
  import context.dispatcher

  var cacheDuration: FiniteDuration = Duration.Zero

  private case class NoCache[V](value: V)
  private case class Cache(key: K, value: V)
  private case class TimeOut(key: K)
  private val cache: Map[K, V] = Map.empty

  override def preStart(): Unit = {
    if (!cacheEnabled)
      logger.debug("Cache is currently disabled")
    else
      logger.debug(s"Cache is set to $cacheDuration")
  }

  override def receive: Receive = {
    case SetCacheDuration(duration) =>
      cacheDuration = duration
      logger.info(s"Cache is set to $cacheDuration")
    case key: K if cache.contains(key) ⇒
      timers.startSingleTimer(key, TimeOut(key), cacheDuration)
      sender ! cache(key)
    case NoCache(v)                ⇒ sender ! v
    case Cache(key, valueResponse) ⇒ onMessageWithResponse(key, valueResponse)
    case TimeOut(key)              => cache -= key
  }

  protected final def respond(futureValue: Future[V]): Unit = futureValue.map(NoCache(_)).pipeTo(self)(sender())
  protected final def cacheAndRespond(key: K, valueFuture: Future[V]): Unit = {
    valueFuture
      .map(Cache(key, _))
      .pipeTo(self)(sender())
    valueFuture
      .recoverWith {
        case t => Future.failed(t)
      }
      .pipeTo(sender())
  }

  private def cacheEnabled: Boolean = cacheDuration != Duration.Zero
  protected def addToCache(key: K, value: V): Unit = {
    if (cacheEnabled) {
      cache += (key -> value)
      timers.startSingleTimer(key, TimeOut(key), cacheDuration)
      logger.debug(s"added to cache : $key -> $value")
    }
  }
  private def onMessageWithResponse(key: K, value: V): Unit = {
    addToCache(key, value)
    sender ! value
  }

  protected def findInCache(value: V): Option[K] = cache.find(_._2 == value).map(_._1)
}
