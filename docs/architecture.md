# Plasma home server architecture

## Overview

Plasma home server (HS) is an implementation of [Matrix API specifications](https://matrix.org/docs/spec/).

Frontend API endpoints are implemented with [Play framework](https://www.playframework.com/). 

Play manages the application bootstrapping, HTTP security and routing to controllers.

Controllers are in charge of handling HTTP request and providing HTTP response. They check authentication and authorization, if needed.
Then also manage JSON request object decoding to scala case class and scala case class response encoding to JSON.

The first backend layer is the service layer. Services are called by the controller layer and are in charge of implementing 
endpoints logic. Services call repositories (ie. [DAO](https://en.wikipedia.org/wiki/Data_access_object)) for querying the database or persisting data. 
Services can also can [actors](https://doc.akka.io/docs/akka/current/general/actors.html) to run asynchronous or distributed tasks. 

Internally, Play starts an [actor system](https://doc.akka.io/docs/akka/current/general/actor-systems.html) which is used to run application of framework actors.
This actor system is configured in a way that it can be part of a [cluster](https://doc.akka.io/docs/akka/current/common/cluster.html#intro) of actor systems and behave like a cluster node. 
Cluster allow actors to be transparently spread among cluster nodes.

That way, one can start several plasma HS instances and configure them to be part of the same cluster. The application is then aware of this 
configuration and the provides high performance, high availability and fault tolerant.   


## Actors architecture

Each matrix room is managed by a room server represented as an actor. They are called by services or other actors for getting the room
state or interacting with it to send events. Room server hold the room state and managed its 
persistence to the database. They are automatically distributed among cluster nodes using [cluster sharding](https://doc.akka.io/docs/akka/current/cluster-sharding.html).

Room servers delegate event management to room workers. When an event is sent to a room server, the room server actor allocate a room worker
and send the event to this worker. Room workers are responsible for validating events, computing room state update and resolving the room event graph.
Room state update is then sent back to the room server.

[Distributed publish subscribe](https://doc.akka.io/docs/akka/current/distributed-pub-sub.html) is used for between room servers and federation actors to 
send/receive events to/from other HS.     


## Technologies       

Plasma is written in [Scala](https://www.scala-lang.org/) which provides object-oriented and functional programming on the JVM. 

Core backend relies on [Akka](https://akka.io/) technologies like actor model messaging, clustering and streaming to provide a [reactive](https://www.reactivemanifesto.org/), scalable and resilient platform.

Data persistence is delegated to [PostgreSQL](https://www.postgresql.org/) database.
 
