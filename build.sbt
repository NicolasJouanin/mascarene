/*
Copyright 2018 Nicolas Jouanin

This file is part of Plasma.

Plasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License 3.

Plasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>
 */
ThisBuild / scalaVersion := "2.13.2"
ThisBuild / organization := "org.mascarene"
ThisBuild / version := "0.1.0-SNAPSHOT"

lazy val root = (project in file("."))
  .aggregate(matrixCoreApi, matrixClientApi, homeServer)
  .settings(
    name := "mascarene"
  )

// Mascarene utilities
lazy val matrixUtils = (project in file("matrix-utils"))
  .settings(
    name := "matrix-utils",
    libraryDependencies := Dependencies.utilsDependencies
  )

// Mascarene SDK
lazy val matrixCoreApi = (project in file("matrix-core-api"))
  .settings(
    name := "matrix-core-api",
    libraryDependencies := Dependencies.commonDependencies
  )

// Matrix client API implementation
lazy val matrixClientApi = (project in file("matrix-client-api"))
  .settings(
    name := "matrix-client-api",
    libraryDependencies := Dependencies.commonDependencies
  )
  .dependsOn(matrixCoreApi, matrixClientApiModel)

// Matrix client API model
lazy val matrixClientApiModel = (project in file("matrix-client-api-model"))
  .settings(
    name := "matrix-client-api-model",
    libraryDependencies := Dependencies.commonDependencies
  )
  .dependsOn(matrixCoreApi)

// Matrix identity API client implementation
lazy val matrixIdentityApi = (project in file("matrix-identity-api"))
  .settings(
    name := "matrix-identity-api",
    libraryDependencies := Dependencies.commonDependencies
  )
  .dependsOn(matrixCoreApi, matrixIdentityApiModel)

// Matrix client API model
lazy val matrixIdentityApiModel = (project in file("matrix-identity-api-model"))
  .settings(
    name := "matrix-identity-api-model",
    libraryDependencies := Dependencies.commonDependencies
  )
  .dependsOn(matrixCoreApi)

// Mascarene Home Server
lazy val homeServer = (project in file("homeserver"))
//.enablePlugins(PlayScala)
  .enablePlugins(BuildInfoPlugin)
  .settings(
    name := "mascarene-homeserver",
    libraryDependencies ++= Dependencies.hsDependencies
  )
  .settings(
    buildInfoPackage := "org.mascarene.homeserver.version",
    buildInfoObject := "BuildInfo",
    buildInfoKeys := Seq[BuildInfoKey](name, version, "projectName" -> "mascarene-hs"),
    buildInfoOptions += BuildInfoOption.BuildTime,
    parallelExecution in Test := false
  )
  .dependsOn(matrixUtils, matrixCoreApi, matrixClientApiModel, matrixClientApi)

onLoad in Global ~= (_ andThen ("project homeServer" :: _))
